## 4.5.9
###### _Mar 31, 2020_
- Aggiunto riferimento al numero del documento in F24 nella colonna degli ordini e nel dettaglio dell'ordine

## 4.5.8
###### _Mar 23, 2020_
- Correzione bug minori

## 4.5.7
###### _Mar 12, 2020_
- Verifica funzionalità per WP 5.4 e WC 4
- Migliorata la gestione dei pulsanti nelle colonne 'Ordine F24' e 'Fattura F24'
- Corretto errore che impediva di caricare le aliquote di spedizione in caso di carrello vuoto

## 4.5.6
###### _Mar 04, 2020_
- Rinomina del file trace.log al click su Download, aggiunto controllo su debug WP attivo
- Corretto bug che impediva il passaggio dei dati fiscali dall'ordine alla fattura
- Modificata la gestione delle aliquote di spedizione: ora vengono prese quelle dei prodotti solo in caso di errore.

## 4.5.5
###### _Feb 26, 2020_
- Migliorata la gestione del flag sul Codice Fiscale
- Migliorata la gestione dei campi fiscali per ordini inseriti lato admin
- Corretto bug su TotalWithoutTax : non veniva conteggiato lo sconto

## 4.5.4
###### _Feb 11, 2020_
- Corretto bug relativo al codice prodotto per i prodotti variabili
- Corretto bug in caso di aliquota di spedizione diversa da aliquota prodotto
- Corretto bug: ora il CustomerName viene popolato solo con i dati di fatturazione
- Revisione e pulizia del codice

## 4.5.3
###### _Gen 22, 2020_
- Corretto bug sulla gestione dei campi aggiuntivi nel checkout per clienti non registrati

## 4.5.2
###### _Gen 20, 2020_
- Cambiato metodo di gestione dei pagamenti elettronici
- Migliorata la gestione del tag FePaymentCode
- Aggiunta gestione della modalità di pagamento 'Assegno'

## 4.5.1
###### _Gen 13, 2020_
- Aggiunta gestione dei dati fiscali del cliente per gli ordini inseriti da admin
- Migliorata la gestione dei pagamenti elettronici
- Migliorata la gestione della Natura Iva per i coupon

## 4.5.0
###### _Gen 07, 2020_
- Modificata la casella di controllo 'Stato Pagato' in un menu a tendina
- Inserito il nome dell'istituto nel tag PaymentMethodName se il metodo prescelto è il bonifico
- Correzione bug minori

## 4.4.8
###### _Dic 12, 2019_
- Corretta gestione Natura IVA per ordini creati da admin
- Corretta azione di salvataggio del codice destinatario per ordini creati lato admin
- Creazione di un documento in stato 'Pagato' se il metodo prescelto è Paypal (o stripe, payplug, ppay)

## 4.4.7
###### _Dic 05, 2019_
- Aggiunta la gestione del piano dei conti nell'ordine
- Migliorata la compatibilità WPML
- Migliorata la leggibilità del codice in api_call.php

## 4.4.6
###### _Nov 26, 2019_
- Migliorati i controlli sulle aliquote dei prodotti
- Aggiunta la gestione del piano dei conti nell'ordine

## 4.4.5
###### _Nov 19, 2019_
- Migliorata la gestione dei prodotti
- Aggiunta la Natura per le commissioni (fees)
- Verificata compatibilità con WP 5.3 e WC 3.8

## 4.4.4
###### _Nov 06, 2019_
- Aggiunti pulsanti di creazione documento nelle colonne di controllo status ordine e fattura
- Migliorata la gestione della natura IVA
- Aggiornata la gestione del metodo di spedizione

## 4.4.3
###### _Ott 30, 2019_
- Migliorata la gestione delle aliquote di spedizione
- Migliorati i testi sorgente e le traduzioni
- Migliorato lo stile della schermata 'Attenzione'

## 4.4.2
###### _Ott 28, 2019_
- Aggiunta gestione del campo vuoto partita IVA per stati esteri
- Corretto bug in descrizione IVA per i coupon
- Disabilitata l'azione 'Invia Email' quando è selezionata la Fattura Elettronica

## 4.4.1
###### _Ott 18, 2019_
- Migliorata la gestione della natura IVA per la spedizione

## 4.4.0
###### _Set 27, 2019_
- Migliorata la gestione delle aliquote di spedizione
- Modificato il testo di suggerimento sotto il campo API Key

## 4.3.9a
###### _Set 25, 2019_
- Migliorata la gestione dei coupon in compatibilità con versioni precedenti di WooCommerce

## 4.3.9
###### _Set 24, 2019_
- Modificata la visualizzazione dei sezionali: ora vengono visualizzate le anteprima
- Migliorata la gestione dei numeratori nell'xml
- Corretto il comportamento della checkbox Salva Cliente

## 4.3.8
###### _Set 23, 2019_
- Corretto bug: non venivano gestiti gli importi delle fees

## 4.3.7
###### _Set 18, 2019_
- Aggiunte colonne di controllo stato per ordine e fattura
- Migliorata la gestione delle imposte nei costi di spedizione

## 4.3.6
###### _Set 16, 2019_
- Corretto bug: non venivano aggiunti i costi di spedizione all'imponibile

## 4.3.5
###### _Set 11, 2019_
- Commentata funzione di controllo stato ordine e fattura

## 4.3.4
###### _Set 09, 2019_
- Aggiornato metodo di gestione coupon
- Aggiunte colonne di controllo stato per ordine e fattura (beta)

## 4.3.3
###### _Set 06, 2019_
- Corretto bug che impediva la creazione dell'ordine in caso di utilizzo coupon
- Modificata gestione dei totali: ora vengono presi da WooCommerce e non calcolati
- Migliorata la gestione dei dati di spedizione
- Corretto bug relativo alla descrizione aliquota IVA

## 4.3.2
###### _Set 02, 2019_
- Corretto bug: il sistema non elencava il metodo di spedizione nell'xml

## 4.3.1
###### _Ago 28, 2019_
- Aliquote IVA: vengono recuperate dai metodi WooCommerce
- Migliorata la gestione Natura IVA, in Configurazione Tassa e nella gestione della FE 
## 4.3.0
###### _Ago 22, 2019_
- Ripristinate azioni Fattura24 nella schermata ordini
- Aggiunto tag di descrizione al metodo di pagamento Paypal
- Aggiunta gestione aliquote IVA con decimali e corretti bug di arrotondamento al centesimo

## 4.2.4
###### _Ago 01, 2019_
- Aggiunto pulsante per causale documento predefinita
- Correzione bug minore

## 4.2.3
###### _Lug 25, 2019_
- Migliorata la gestione dei metodi di pagamento

## 4.2.2
###### _Lug 08, 2019_
- Modificate le etichette dei campi aggiuntivi

## 4.2.1
###### _Lug 01, 2019_
- Migliorata gestione azioni aggiuntive nella schermata degli ordini

## 4.2.0
###### _Lug 01, 2019_
- Aggiunto Tab notizie importanti
- Ripristino dei link nella sezione ordini
- Aggiunto ai link supporto per la FE
- Correzione bug minori