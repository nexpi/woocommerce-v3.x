=== Fattura24 ===

Contributors:      Fattura24
Plugin Name:       Fattura24
Plugin URI:        http://www.fattura24.com
Tags:              fattura elettronica, fatture, fattura xml, codice destinatario, woocommerce
Author URI:        http://www.fattura24.com
Author:            Fattura24.com
Requires PHP:      5.6
Requires at least: 4.6
Tested up to:      5.4
Stable tag:        4.5.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


The official <b>Fattura24 plugin</b> enables <b>the creation of electronic invoices, orders, traditional invoices and receipts</b> via [Fattura24.com.] cloud service ( https://www.fattura24.com)

== Description ==

The official <b>Fattura24 plugin</b> enables <b>the creation of electronic invoices, orders, traditional invoices and receipts</b> via [Fattura24.com.] cloud service ( https://www.fattura24.com)
You can also analyze the progress of your business by graphic reports and share everything with your accountant.
You can hook one or more eCommerce to your Fattura24 account and on each one create documents with different issue counters.

By the plugin you can:

* Add the PEC and SDI Recipient Code fields on the check-out page;
* Add the fields "Tax code" and "VAT number" of the customer on the check-out page and decide if the fields have to be required;
* Create a copy of the order from WooCommerce to Fattura24;
* Send an automatic email to the customer with a PDF copy of the order or invoice attached;
* Add the customer information to your Fattura24 address book or update the data if they have already been created;
* Create the receipt/invoice - in your Fattura24 account - relating to the order and download it to your e-commerce so that it can be displayed both by the administrator and the customer. The system will create an invoice if the customer has filled out the VAT number field, otherwise a receipt, or you can choose to create the invoice always;
* Load stocks in Fattura24, pledge the goods with orders and unload them with invoices;
* Choose the template to create PDF copies of orders and receipts / electronic invoices / invoices;
* Associate the current-account balance for each invoice and analyze the details through the Fattura24 graphic reports;
* Set a custom issue counter for the invoices.

== Installation ==

<h4>Preparing</h4>
To use Fattura24 plugin as a billing service in WooCommerce you need:<br/>

   <ul><li>"Business" subscription on Fattura24</li>
   <li>API KEY (you can find it on your Fattura24 account)</li></ul>

to get the API Key go to ‘Configurazione’ -> ’App Servizi Esterni’, click on "SI" to enable and copy the key.

<h4>Configuration</h4>
Now let's see how to configure the Fattura24 plugin in Wordpress admin area:
   <ul>
   <li>go to ‘Plugins’ -> ’Add new’</li>
   <li>use the search toolbar to quickly find "Fattura24"</li>
   <li>then click on "Install Now"</li>
   <li>fill out the API Key field</li>
   <li>then click on ‘Save settings’</li>
   <li>click on ‘Verifica API KEY’ to make sure you've entered the correct API KEY</li>
   </ul>

<h4>Verification</h4>
Your configuration is done, let's proceed with some tests.
Whenever you receive an order from your check-out page, the plugin will do the following:

   * if you checked 'Save customer', it will add the customer to Fattura24 address book or update its data;
   * if you checked 'Create order', it will send the order to Fattura24;
   * if you checked 'Send email', an email will be sent to the customer with the order.

When you set the status of the order on 'Processing' or ‘Completed’, the plugin will do the following:

   * if you selected to Create an invoice, it will create a receipt in Fattura24 if the customer doesn't have a VAT number, otherwise it will create an invoice;
   * it will add the customer to your address book or update the data;
   * it will create the PDF receipt / invoice 
   * if you checked 'Download PDF' it download a copy of the PDF on your e-commerce;
   * if you checked 'Send email', an email will be sent to the customer with the receipt / invoice PDF;
   * the Invoice will be created (or not) in the 'paid' status according to the option you chose;
   * if you checked 'Disable receipts', an invoice will be created instead of a receipt even in the absence of the VAT number.


The plugin also adds the fields "Tax Code" and "VAT number" to the customer data. You can choose whether or not the customer is required to fill these fields.

Using Fattura24 with WooCommerce is very simple but if you would like technical assistance contact us at +39 06-40402261.

== Changelog ==

= 4.5.9 =
* Aggiunto riferimento al numero del documento in F24 nella colonna degli ordini e nel dettaglio dell’ordine

= 4.5.8 =
* Correzione bug minori

= 4.5.7 =
* Verifica funzionalità per WP 5.4 e WC 4
* Migliorata la gestione dei pulsanti nelle colonne 'Ordine F24' e 'Fattura F24'
* Corretto errore che impediva di caricare le aliquote di spedizione in caso di carrello vuoto

= 4.5.6 =
* Rinomina del file trace.log al click su Download
* Corretto bug che impediva il passaggio dei dati immessi nei campi fiscali dall'ordine alla fattura
* Modificata la gestione delle aliquote di spedizione: ora vengono prese quelle dei prodotti solo in caso di errore.

= 4.5.5 =
* Migliorata la gestione del flag sul Codice Fiscale
* Migliorata la gestione dei campi fiscali per ordini inseriti lato admin
* Corretto bug su TotalWithoutTax : non veniva conteggiato lo sconto

= 4.5.4 =
* Revisione e pulizia del codice
* Corretto bug relativo al codice prodotto per i prodotti variabili
* Corretto bug in caso di aliquota di spedizione diversa da aliquota prodotto
* Corretto bug: ora il CustomerName viene popolato solo con i dati di fatturazione

== Frequently Asked Questions ==

= Is the plugin free? =

To create PDFs and electronic invoices you need a Business or Enterprise subscription on Fattura24.
More information on Fattura24 prices are available [here] (https://www.fattura24.com/prezzi-versioni-fattura24/)

= Do you have a customer service? =

Yes, the customer support service is available for all the subscription plans, it is provided by Italy with Italian personnel.
Contact us +39 06-40402261

= How can get a trial period of Fattura24? =

You can use this code coupon to register on Fattura24 and activate a business plan for one month for free:
Coupon: WOOHAPPY or click [here] (https://www.app.fattura24.com/html/subscription.html?coupon=WOOHAPPY)
Do you need more time? Call us and we will extend the duration of your trial period.

== Screenshots ==

1. Click on: Configurazione -> App e Servizi esterni
2. Then click on WooCommerce icon
3. Copy your API KEY
4. Paste API KEY and save the settings, then verify the key
5. Set the plugin to create Electronic Invoices (example)
6. Remember to specify Natura for 0% rates by following these instructions
7. To create the document, put the order to the status selected in settings page (e.g.: "Completed")
8. Columns to check document status in Fattura24