<?php
/*
Plugin Name: Fattura24
Plugin URI: http://www.fattura24.com
Description: Create your invoices with Fattura24.com
Version:  4.5.9
Text Domain: fattura24
Domain Path: /languages
Author: Fattura24.com
License: GPL2
License URI : http://www.gnu.org/licenses/gpl-2.0.html
*/

namespace fattura24;
if (!defined('ABSPATH')) exit;
require_once 'src/behaviour.php';

if (is_admin()) {
	require_once(ABSPATH . "wp-admin/includes/screen.php");
	if(function_exists('get_current_screen')){
		$screen = get_current_screen();
	}
    require_once 'src/uty.php';
    require_once 'src/hooks.php';
    require_once 'src/settings.php'; //pagina settings principale
    require_once 'src/tax.php';  // pagina tax
    require_once 'src/app.php'; // pagina app
    require_once 'src/warning.php'; // pagina warning

    // codice eseguito solo lato admin
    add_action('admin_menu', function() {
        add_options_page(
            __('Settings Fattura 24', 'fattura24'), 'Fattura24',
            'manage_options',
            FATT_24_SETTINGS_PAGE,
            __NAMESPACE__.'\fatt_24_show_settings'
        ); // schermata di impostazioni principale
        add_submenu_page(  // tab "Configurazione Tassa"
            null, 
            'Settings Fattura 24', 
            'Fattura24 Tax',
            'manage_options', 
            'fatt-24-tax',
            __NAMESPACE__.'\fatt_24_show_tax'
        );
        add_submenu_page( // tab "App"
            null, 
            'Settings Fattura 24', 
            'Mobile App',
            'manage_options', 
            'fatt-24-app',
            __NAMESPACE__.'\show_app'
        );  
        add_submenu_page( // tab "Attenzione"
            null, 
            'Settings Fattura 24', 
            'Warning',
            'manage_options', 
            'fatt-24-warning',
            __NAMESPACE__.'\show_warning'
        );            
    });
	
    add_action('admin_init', __NAMESPACE__.'\fatt_24_init_settings');
    
    // link al sito fattura24.com, $page definisce un link interno
    function fatt_24_webase($page) { 
        return 'http://www.fattura24.com/'.$page; 
    }
    
    // con questo metodo includo nele impostazioni (in tutte le tab) i link alla documentazione, il logo F24 e il pulsante "Accedi", usando gli helpers html. Attualmente $topic è sempre null, altrimenti verrebbe usato per l'intestazione h1
    function fatt_24_get_link_and_logo($topic = null, $ico = 'logo_orange') {
        echo fatt_24_div(array($topic ? fatt_24_h1($topic) : '',
             fatt_24_div(fatt_24_style(array('float'=>'right', 'margin-top'=> '0px', 'padding'=>'20px')), 
             array(fatt_24_a(array('href'=>fatt_24_webase(''), 'target' => '_blank'), __('Support', 'fattura24')), '|',
                   fatt_24_a(array('href'=>fatt_24_webase('woocommerce-plugin-fatturazione'), 'target' => '_blank'), __('Documentation', 'fattura24')), '<br/>',
                   fatt_24_a(array('href'=>fatt_24_webase('termini-utilizzo'), 'target' => '_blank'), __('Fattura24 general terms and conditions of contract', 'fattura24')), '<br/>',
                   fatt_24_a(array('href'=>fatt_24_webase('regolamento-ecommerce'), 'target' => '_blank'), __('Supplementary F24 regulation for WooCommerce plugin', 'fattura24')), '<br/>',
                   fatt_24_a(array('href'=>fatt_24_webase('policy'), 'target' => '_blank'), __('Privacy policy', 'fattura24')), '<br/>',
             )),
             fatt_24_div(fatt_24_style(array('padding' =>'20px')),(fatt_24_img(fatt_24_attr('src', fatt_24_png('../assets/'.$ico)), array()))),
             fatt_24_a(array('padding' => '20px','class' => 'button button-primary', 'href' => 'https://www.app.fattura24.com/html/index.html', 'target' => '_blank'), __('Log in Fattura24', 'fattura24'))
             ));
        
    }
  
    // il plugin funziona solo con WooCommerce installato
    //  visualizza un avviso in caso contrario
    add_action('admin_notices', function() {
        if (!class_exists('WooCommerce'))
            echo fatt_24_div(fatt_24_klass('updated'), fatt_24_p(__('Fattura 24 plugin requires WooCommerce installed', 'fattura24')));
    });
    // chiamata Ajax verifica API Key
    add_action('wp_ajax_api_verification', function() {
        check_ajax_referer(FATT_24_API_VERIF_NONCE, 'security');
        wp_send_json(fatt_24_verifica_api_key(sanitize_text_field($_POST['api_key'])));
    });
    // include script generazione PDF
    add_action('admin_enqueue_scripts', function() {
        $js = 'src/f24_pdfcmd.js';
        wp_enqueue_script('f24_pdfcmd', plugins_url($js, __FILE__), array('jquery'));
    });
}
else {
    require_once 'src/hooks.php';
}
register_activation_hook(__FILE__, function() {
    // aggiungo una tabella al db wp
    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'fattura_tax';
    $sql = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `tax_id` int(11) NOT NULL DEFAULT '0',
    `tax_code` varchar(255) DEFAULT NULL,
    `dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `mo_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `extra_note` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
    )$charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);

    // con questo carico il textdomain fattura24 e gestisco le traduzioni in lingua in conformità con WPML
    function fatt_24_textdomain()
    {
        load_plugin_textdomain('fattura24', false, basename(dirname(__FILE__)) . '/languages');
    }
    add_action('plugins_loaded', 'fatt_24_textdomain');
});

// con questo metodo gestisco il download del file di log
add_action('wp_ajax_hwedonwloadlogs', function (){
    if(!current_user_can('manage_options')) 
        return;

    if (!wp_verify_nonce( sanitize_text_field($_POST['nonce']), 'hwe_verify_custom_nonce')){ 
        die( "page killed" ); 
    }
	
	$data = sanitize_text_field($_POST['info']);
	if( WP_DEBUG === true ) { 
		if ($f = @fopen(dirname(__FILE__) . '/src/trace.log', 'a'))
		{
			fprintf($f, "\n%s\n", $data);
			fclose($f);
		}
		echo json_encode(array('t'=>1));
		exit();
	}
	else{
		echo json_encode(array('t'=>2));
		exit();
	}
});

// con questo metodo gestisco la cancellazione del file di log
add_action('wp_ajax_hwe_deletelogs', function(){
	
    if(!current_user_can('manage_options')) 
        return;

	if ( ! wp_verify_nonce( sanitize_text_field($_POST['nonce']), 'hwe_delete_logs_nonce' ) ) { 
        die( "page killed" ); 
    }
	$response = array('status'=>false);
	$filePath = dirname(__FILE__) . '/src/trace.log'; 
		
	if(file_exists($filePath))
	{
		$response['fileExists'] = true;
		$response['size'] = round(filesize($filePath) / 1000.);
		unlink($filePath);
	}
	else
	{
	    $response['fileExists'] = false;
	}
	$response['path'] = $filePath;
	$response['status'] = true;
	echo json_encode($response);
	wp_die();
});

