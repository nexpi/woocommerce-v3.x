<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * File di gestione delle chiamate API
 * conversione dei dati dell'ordine in formato xml
 */
namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'tax.php'; // così recupero tax_id e le altre variabili
require_once 'uty.php';
require_once 'constants.php';

//restituisce il valore di una checkbox
function fatt_24_get_flag($key)
{
	$val = get_option($key, false);
	$ret = $val ? true : false;
	return $ret;
}
 
// restituisce il valore di una checkbox (con procedimento literal)
function fatt_24_get_flag_lit($key)
{
	return fatt_24_get_flag($key) ? 'true' : 'false';
}
/**
*  chiamata API F24 @param $command (es.: SaveDocument) e @param $send_data 
*  usa il metodo wp_remote_post
*/ 
function fatt_24_api_call($command, $send_data)
{
	$url = implode('/', array(FATT_24_API_ROOT, $command));
	if (!isset($send_data['apiKey']))
		$send_data['apiKey'] = get_option(FATT_24_OPT_API_KEY);
			
	$response = wp_remote_post( $url, array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => array(),
				'body' => $send_data,
				'cookies' => array()
				)
	);
	$body = wp_remote_retrieve_body($response);
	return  $body;
}

// verifica API key
function fatt_24_verifica_api_key($apiKey = null)
{
	$res = fatt_24_api_call('TestKey', array('apiKey'=>$apiKey));
	$res = str_replace('&egrave;', 'è', $res);
	$ans = simplexml_load_string($res);
	if (is_object($ans))        // risposta alla chiamata API
		$APIStatus = array(
			'returnCode' => intval($ans->returnCode),
			'description' => strval($ans->description)
		);
	else
		$APIStatus = array(
			'returnCode' => '?',
			'description' => __('generic error, please contact our technical service at <info@fattura24.com>','fattura24')
		);
	return $APIStatus;
}

/**
* Funzione che ne chiama altre per gestire le varie azioni di salvataggio documento in F24
* @param $order_id
* @param $docType 
*/
function fatt_24_store_fattura24_doc($order_id, $docType)
{
	$xml = fatt_24_order_to_XML($order_id, $docType); // chiamo funzione che converte l'ordine in xml
	$res = fatt_24_api_call('SaveDocument', array('xml' => $xml)); // chiamo le API per salvare il documento
	fatt_24_trace('Store Fattura24 Doc', $order_id, $docType, $xml, $res); // registro tutto nel tracciato

	$status = fatt_24_get_order_status($order_id); // leggo lo stato dell'ordine
	$ans = simplexml_load_string($res); // risposta alla chiamata API
	if (is_object($ans))
	{
		fatt_24_order_status_set_doc_data($status, intval($ans->returnCode), strval($ans->description), intval($ans->docId), $docType, strval($ans->docNumber));
		$rc = true;
	}
	else
	{
		fatt_24_order_status_set_error($status, sprintf(__('Unknow error occurred while uploading order %d', 'fattura24'), $order_id));
		$rc = false;
	}
	fatt_24_store_order_status($order_id, $status);
	return $rc;
}

// scarico il PDF in locale
function fatt_24_download_PDF($order_id)
{
	/**
	 * con $docParam passo alla funzione fatt_24_is_available_on_f24
	 * il valore scelto dall'utente nelle impostazioni - nella sezione "Crea fattura"
	 * Davide Iandoli 28.06.2019
	 */
	$fatt_inv_create_new = get_option("fatt-24-inv-create");
	if ($fatt_inv_create_new == 1) {
		$docParam = FATT_24_DT_FATTURA;
	} else if ($fatt_inv_create_new == 2) {
		$docParam = FATT_24_DT_FATTURA_ELETTRONICA;
	}

	$docResult = fatt_24_is_available_on_f24($order_id, $docParam);  // controllo se il documento è stato già creato
	$docId = $docResult['docId']; // registro il docId se è presente
	$rc = false;
	if ($docId)
	{
		$status = fatt_24_get_order_status($order_id);
		$PDF = fatt_24_api_call('GetFile', array('docId' => $docId)); //chiamo le API per scaricare il documento
		if (substr($PDF, 0, 4) == '%PDF')
		{
			$status = apply_filters(FATT_24_DOC_STORE_FILE, $status, $order_id, $PDF);
			$rc = true;
		}
		else
		{
			$ans = simplexml_load_string($PDF);
			if ($ans)
				fatt_24_order_status_set_error($status, !is_object($ans) ? error_get_last() : strval($ans->description));
			else
				fatt_24_order_status_set_error($status, sprintf(__('Unknow error occurred while downloading PDF for order %d', 'fattura24'), $order_id));
		}
		fatt_24_store_order_status($order_id, $status);
	}
	return $rc;
}

/**
* Con queste funzioni calcolo l'aliquota dal prezzo e dall'importo dell'IVA
* utilizzate solo se mi arrivano dati non congruenti (es: $vat = 0 e $total_tax > 0)
* Davide Iandoli 19.11.2019
*/
function fatt_24_vat_from_price_and_tax($PriceWithout, $VatPaid)
{
	$Total = $PriceWithout + $VatPaid;
	if ($PriceWithout != 0)
	{    
		$Perc = ($Total - $PriceWithout) * 100.0 / $PriceWithout;
		return round($Perc, 2);
	}
	return 0;
}

function fatt_24_price_without_vat($Price, $VatPerc)
{
	$X = 100.0 / $VatPerc;
	$Y = $X + 1;
	$Parz = $Price / $Y;
	return $Price - $Parz;
}

// toglie spazi in eccesso
function fatt_24_make_strings($v1, $v2)
{
	$s = trim(implode(' ', $v1));
	if ($s == '' && !empty($v2))
		$s = trim(implode(' ', $v2));
	return $s;
}

// converte l'ordine in xml
function fatt_24_order_to_XML($order_id, $docType)
{
	$order = new \WC_Order($order_id);
	$order_id = trim(str_replace('#', '', $order->get_order_number()));  
	$DATAMODIFICA = fatt_24_now();
	$user = $order->get_user();
	$user_id = $order->get_user_id(); // mi serve l'user_id per gestire correttamente i dati fiscali dell'utente in caso di ordine inserito da admin - Davide Iandoli 9.01.2020
	$couponApplied = false;
	$couponDiscounts = array();
	$taxTotals = array();
	$taxNames = array();
    
	$fixnum = function ($n, $p) { // assegno alla variabile il risultato della funzione
		$n = round($n, $p);
		return number_format($n, $p, '.', ''); // cambio metodo di arrotondamento senza separatore migliaia
	};
	
	$billing_address = $order->get_formatted_billing_address();
	$shipping_address = $order->get_formatted_shipping_address();
	$Email = $order->get_billing_email();
	if (!$Email)
		if ($user)
			$Email = $user->user_email;

	$CellPhone = $order->get_billing_phone();
	$Address = apply_filters(FATT_24_DOC_ADDRESS, $order);
	$Postcode = fatt_24_make_strings(array($order->get_billing_postcode()),
		array($order->get_shipping_postcode()));
	$City = fatt_24_make_strings(array($order->get_billing_city()),
		array($order->get_shipping_city()));
	$Province = fatt_24_make_strings(array($order->get_billing_state()),
		array($order->get_shipping_state()));
	$Country = WC()->countries->countries[fatt_24_make_strings(array($order->get_billing_country()),
		array($order->get_shipping_country()))];
	$BillingCountry = $order->get_billing_country(); // aggiunto indirizzo di fatturazione per controllo campi SDI e PEC Davide Iandoli 14.02.2019
	 
	//cerco i campi fiscali nell'ordine; se non li trovo li cerco nell'utente per gestire correttamente i casi di ordine creato da admin
	if($BillingCountry != 'IT'){
		$FiscalCode = ''; // campo vuoto per l'estero
	} else {
		$FiscalCode = !empty(fatt_24_order_c_fis($order)) ? strtoupper(fatt_24_order_c_fis($order)) : strtoupper(fatt_24_user_fiscalcode($user_id)); // cf maiuscolo
	}	

	$VatCode = !empty(fatt_24_order_p_iva($order)) ? fatt_24_order_p_iva($order) : fatt_24_user_vatcode($user_id); 
	$Recipientcode = !empty(fatt_24_order_recipientcode($order)) ? fatt_24_order_recipientcode($order) : fatt_24_user_recipientcode($user_id);
	$Recipientcode = (!empty($Recipientcode) ? $Recipientcode : '0000000');
	$Pecaddress = !empty(fatt_24_order_pecaddress($order))? fatt_24_order_pecaddress($order) : fatt_24_user_pecaddress($user_id);
	$Name = fatt_24_make_strings(array($order->get_billing_company()), ''); //Mi interessa solo il nome di fatturazione; passo una stringa vuota come secondo argomento perché la funzione chiede due parametri 

	if (empty($Name))
		$Name = fatt_24_make_strings(array($order->get_billing_first_name(), 
										   $order->get_billing_last_name()),
										   array($order->get_shipping_first_name(), $order->get_shipping_last_name()));
		
	if(!empty($BillingCountry) && $BillingCountry != 'IT')	{
		if(empty($VatCode))
			$VatCode = $Name; // inserisco il nome o la ragione sociale se i campi fiscali sono vuoti
	}	
	
	$xml = new \XMLWriter();
	if (!$xml->openMemory())
		throw new \Exception(__('Cannot openMemory', 'fattura24'));
	
	$xml->startDocument('1.0', 'UTF-8');
	$xml->setIndent(2);
	$xml->startElement('Fattura24');
	$xml->startElement('Document');
	
	$field = function ($v, $max) {
		return substr($v, 0, $max);
	};
		
	$customerData = array(
		'Name' => $Name,
		'Address' => $field($Address, FATT_24_API_FIELD_MAX_indirizzo),
		'Postcode' => $field($Postcode, FATT_24_API_FIELD_MAX_cap),
		'City' => $field($City, FATT_24_API_FIELD_MAX_citta),
		'Province' => $field($Province, FATT_24_API_FIELD_MAX_provincia),
		'Country' => $order->get_billing_country(), // passa il valore del paese di fatturazione Davide Iandoli 14.02.2019
		'CellPhone' => $CellPhone,
		'FiscalCode' => $FiscalCode,
		'VatCode' => $VatCode,
		'Email' => $Email,
	);
	$customerData = apply_filters(FATT_24_CUSTOMER_USER_DATA, $customerData);
	
	$DeliveryName = $order->get_shipping_company();
	if (empty($DeliveryName))
		$DeliveryName = trim($order->get_shipping_first_name() . " " . $order->get_shipping_last_name());
	$DeliveryAddress = trim(trim($order->get_shipping_address_1()) . " " . trim($order->get_shipping_address_2()));
	$DeliveryPostcode = $order->get_shipping_postcode();
	$DeliveryCity = $order->get_shipping_city();
	$DeliveryProvince = $order->get_shipping_state();
	$DeliveryCountry = $order->get_shipping_country();
			
	$customerDeliveryData = array(
		'Name' => $DeliveryName,
		'Address' => $DeliveryAddress,
		'Postcode' => $DeliveryPostcode,
		'City' => $DeliveryCity,
		'Province' => $DeliveryProvince,
		'Country' => $DeliveryCountry,
	);
	/**
	* Controllo Codice SDI: se $BillingCountry != 'IT' => XXXXXX
	* Se vuoto e $BillingCountry == 'IT' => 0000000
	* Altrimenti si prende l'input - Davide Iandoli 06.03.2019
	*/
	
	$xml->writeElement('FeCustomerPec', $Pecaddress);
	if ($BillingCountry != 'IT') {
	   $xml->writeElement('FeDestinationCode', 'XXXXXXX'); // se è estero sette volte X
	} else {   
	   $xml->writeElement('FeDestinationCode', $Recipientcode);
	}
		
	$DocumentType = '';
	if ($docType == FATT_24_DT_ORDINE) {
		$DocumentType = FATT_24_DT_ORDINE;
		$SendEmail = fatt_24_get_flag_lit(FATT_24_ORD_SEND);
		$updateStorage = get_option(FATT_24_ORD_STOCK);
			
		if (empty($customerDeliveryData['Address']))
			$template = get_option(FATT_24_ORD_TEMPLATE);
		else
			$template = get_option(FATT_24_ORD_TEMPLATE_DEST);
		
	} else {
		$fatt_24_inv_object = sanitize_text_field(get_option("fatt-24-inv-object")); // Causale personalizzata dal campo opzioni
		$fatt_24_send_object = str_replace("(N)", $order_id, $fatt_24_inv_object); // Sostituisco (N) con il numero d'ordine
		$xml->writeElement('Object', $fatt_24_send_object);
		$fatt_24_paid_status = get_option("fatt-24-inv-create-when-paid"); // qui mi prendo il valore del menu a tendina per lo stato "Pagato"
		$fatt_24_set_paid_status = $fatt_24_paid_status == 1? 'true' : 'false'; // variabile inizializzata a true o false in base alla scelta ("Sempre" => true);
			
		if($docType == FATT_24_DT_FATTURA){
				
			if (fatt_24_get_flag_lit(FATT_24_INV_DISABLE_RECEIPTS) == "true")
				$DocumentType = FATT_24_DT_FATTURA_FORCED;
			else
				$DocumentType = $VatCode ? FATT_24_DT_FATTURA : FATT_24_DT_RICEVUTA;
			
		} else if($docType == FATT_24_DT_FATTURA_ELETTRONICA){
			if (fatt_24_get_flag_lit(FATT_24_INV_DISABLE_RECEIPTS) == "true")
				$DocumentType = FATT_24_DT_FATTURA_ELETTRONICA;
			else
				$DocumentType = $VatCode ? FATT_24_DT_FATTURA_ELETTRONICA : FATT_24_DT_RICEVUTA;
		}
			
		if ($DocumentType == FATT_24_DT_RICEVUTA)
			$numerator = get_option(FATT_24_INV_SEZIONALE_RICEVUTA);
		else
			$numerator = get_option(FATT_24_INV_SEZIONALE_FATTURA);
		
	
		if ($numerator !== 'Predefinito') {
			$xml->writeElement('IdNumerator', $numerator);
		}
		$SendEmail = fatt_24_get_flag_lit(FATT_24_INV_SEND);
		$updateStorage = get_option(FATT_24_INV_STOCK);
			
		if (empty($customerDeliveryData['Address'])){
			$template = get_option(FATT_24_INV_TEMPLATE);
			//fatt_24_trace('template 1:', $template);
		}else{
			$template = get_option(FATT_24_INV_TEMPLATE_DEST);
			//fatt_24_trace('template 2:', $template);
		}	
		$xml->writeElement('F24OrderId', fatt_24_is_available_on_f24($order_id, FATT_24_DT_ORDINE));
	}
		
	$xml->writeElement('DocumentType', $DocumentType);
	$xml->writeElement('SendEmail', $SendEmail);
	$xml->writeElement('UpdateStorage', $updateStorage);
		
	if ($template !== 'Predefinito') {
		$idTemplate = trim($template);
		//$idTemplate = rtrim(end(explode(' (ID: ', $template)), ')');
		$xml->writeElement('IdTemplate', $idTemplate);
	}
		
	foreach ($customerData as $k => $v){
		$xml->writeElement('Customer' . $k, $v);
	}    
	
	foreach ($customerDeliveryData as $k => $v)
		if (!empty($v))
			$xml->writeElement('Delivery' . $k, $v);
		
	$fatt_inv_create_new = get_option("fatt-24-inv-create");
	$payment_method = $order->get_payment_method();
	//fatt_24_trace('metodo pagamento :', $payment_method);
				
	/**
	 * Tramite $payment_method_needle gestisco i pagamenti elettronici e lo stato "pagato"
	 * cercando nel metodo utilizzato la stringa descrittiva (es.: paypal)
	 * Davide Iandoli 20.01.2020
	 */
	if (strpos(strtolower($payment_method), 'paypal') !== false) // converto le stringhe in minuscole con strtolower()
		$payment_method_needle = 'paypal';
	else if (strpos(strtolower($payment_method), 'braintree') !== false)
		$payment_method_needle = 'braintree';
	else if	(strpos(strtolower($payment_method), 'stripe') !== false)
		$payment_method_needle = 'stripe';
	else if (strpos(strtolower($payment_method), 'ppay') !== false)
		$payment_method_needle = 'postepay';
	else if(strpos(strtolower($payment_method), 'payplug') !== false)
		$payment_method_needle = 'payplug';
	else if(strpos(strtolower($payment_method), 'payment') !== false || strpos(strtolower($payment_method), 'wallet') !== false) //altri pagamenti elettronici, aggiunti il 02.04.2020
		$payment_method_needle = 'pagamento smart o con carta';
	else
	    $payment_method_needle = '';				

	$payment_method_title = $order->get_payment_method_title();
	
	// pagamento a mezzo bonifico
	if ($payment_method == 'bacs'){
		$fepaymentcode = 'MP05';
		if ($docType == FATT_24_DT_FATTURA_ELETTRONICA)
		    $xml->writeElement('FePaymentCode', $fepaymentcode); // passo il codice solo se il doc è FE
		$bacs_accounts1 = get_option( 'woocommerce_bacs_accounts' ); //cerca i dati in WooCommerce bacs accounts
			
		if ( !empty( $bacs_accounts1 ) ) { 
			foreach ( $bacs_accounts1 as $bacs_account1 ) {
				$PaymentMethodName = $bacs_account1['bank_name']; // nome della banca
				$xml->writeElement('PaymentMethodDescription', $bacs_account1['iban']); // iban
			}
		}
		/**
		 * Blocco spostato qui per impostare il tag PaymentMethodName anche nel caso i dati in Woocommerce siano vuoti
		 */
		if(!empty($payment_method_title) && empty($PaymentMethodName)){
			$PaymentMethodName = $payment_method_title. '- '.$payment_method;
		} elseif (empty($payment_method_title)) {
			$PaymentMethodName = $payment_method;
		}
		$xml->writeElement('PaymentMethodName', $PaymentMethodName);
		
	// pagamento con assegno
	} elseif($payment_method == 'cheque') { 
		$fepaymentcode = 'MP02';
		if ($docType == FATT_24_DT_FATTURA_ELETTRONICA)
			$xml->writeElement('FePaymentCode', $fepaymentcode); 
	
		$PaymentMethodName = 'Assegno';
		$xml->writeElement('PaymentMethodName', $PaymentMethodName);
		$xml->writeElement('PaymentMethodDescription', 'Pagamento con assegno');
		
	// pagamenti elettronici => uso la variabile $payment_method_needle	
	} elseif(!empty($payment_method_needle)) {
		$fepaymentcode = 'MP08';
		if(!empty($payment_method_title)){
			$PaymentMethodName = ucfirst($payment_method_needle); // voglio la prima lettera maiuscola
			$PaymentMethodDescription = $payment_method_title;
		} else {
			$PaymentMethodName = $payment_method;
		}
		
		if ($docType == FATT_24_DT_FATTURA_ELETTRONICA)
		    $xml->writeElement('FePaymentCode', $fepaymentcode);
		$xml->writeElement('PaymentMethodName', $PaymentMethodName);
		$xml->writeElement('PaymentMethodDescription', $PaymentMethodDescription); // aggiungo la descrizione in caso di pagamento con PayPal
	
	// pagamento alla consegna (in tutti gli altri casi)
	} else {
		$fepaymentcode = 'MP01';
		if(!empty($payment_method_title)){
			$PaymentMethodName = $payment_method_title. '- '.$payment_method;
		} else {
			$PaymentMethodName = $payment_method;
		}
			
		if ($docType == FATT_24_DT_FATTURA_ELETTRONICA)
		    $xml->writeElement('FePaymentCode', $fepaymentcode);
		$xml->writeElement('PaymentMethodName', $PaymentMethodName);
		$xml->writeElement('PaymentMethodDescription', 'Pagamento alla consegna'); // descrizione per pagamento in contrassegno
	}
		
	if ($docType == FATT_24_DT_ORDINE)
		$xml->writeElement('Number', $order->get_order_number());
			
	$order_shipping_total = $order->get_shipping_total(); //totale di spedizione
	$order_shipping_tax = $order->get_shipping_tax(); // tassa applicata alla spedizione
	$order_total_discount = $order->get_discount_total(); // totale sconto, lo sottraggo al totale e al subtotale
		
	$TotalWithoutTax = $fixnum($order->get_subtotal() + $order_shipping_total - $order_total_discount, 2); // aggiungo il totale di spedizione e tolgo i coupon
	$VatAmount = $fixnum($order->get_total_tax(), 2); 
	$Total = $fixnum($order->get_total(), 2); 
	 
	$xml->writeElement('TotalWithoutTax', $fixnum($TotalWithoutTax, 2));
	$xml->writeElement('VatAmount', $fixnum($VatAmount, 2));
	$xml->writeElement('Total', $fixnum($Total, 2));
	
	$FootNotes = apply_filters(FATT_24_DOC_FOOTNOTES, $order);
	$xml->writeElement('FootNotes', $FootNotes);
		
	// tag riportati solo in fattura
	if ($docType != FATT_24_DT_ORDINE)
	{
		$xml->startElement('Payments');
		$xml->startElement('Payment');
		$xml->writeElement('Date', fatt_24_now('Y-m-d'));
		$xml->writeElement('Amount', $fixnum($Total, 2));
			
		// Sovrascrivo la variabile $fatt_24_set_paid_status solo se ho scelto "Pagamenti Elettronici"
		if($fatt_24_paid_status == 2)
		 	if(!empty($payment_method_needle))
				$fatt_24_set_paid_status = 'true'; // se rientra nei pagamenti elettronici è true
		    else
				$fatt_24_set_paid_status = 'false';	// altrimenti è false  
				    
		$xml->writeElement('Paid', $fatt_24_set_paid_status);
		$xml->endElement(); // tag xml Payment
		$xml->endElement(); // tag xml Payments
	}
		
	$xml->startElement('Rows');
	//fatt_24_get_flag_lit(FATT_24_INV_CREATE);
	
	$pdc = get_option(FATT_24_INV_PDC);
	if(!empty($pdc) && $pdc != 'Nessun Pdc')
		$idPdc=rtrim(end(explode(' (ID: ', $pdc)), ')');
				
	// ciclo tutti gli items dell'ordine
	foreach ($order->get_items() as $item_key => $item_value) {
			
		$item_data = $item_value->get_data(); // dati dell'item WooCommerce in array
		fatt_24_trace ('Item data :', $item_data);
			
		$product_id = $item_data['product_id'];
		$product = wc_get_product($product_id);
		$wc_product = $item_value->get_product(); // prodotto in WooCommerce
		$sku = '';
		if(!empty($wc_product))
			$sku = $wc_product->get_sku();
		$product_type = $item_value->get_type(); // tipo di prodotto (simple, variable, subscription...)
		$item_name = $item_data['name'];
		$qty = $item_data['quantity'];
		$price = $fixnum($item_data['subtotal'] / $qty, 6); // prezzo unitario, 6 decimali
			 		
		//ottengo l'aliquota dal prodotto (attenzione: la "\" è per il corretto riferimento alla cartella)
		$tax_rates = \WC_Tax::get_base_tax_rates($product->get_tax_class(true));
		if(!empty($tax_rates))
			$tax_rate = reset($tax_rates);
			
		// cerco la natura nella colonna del db - in questo modo gestisco la natura anche per ordini creati lato admin	
		$vatNature = fatt_24_getNatureColumn(); 
		$vat = $tax_rate['rate'];
		$vatDescription = $tax_rate['label']; 
		$taxNames[$vat] = $vatDescription; // creo un array associativo per le descrizioni, lo userò per i coupon
			 
		/* 
		gestisco prodotti che in Woocommerce sono configurati come "Esente" e "Shipping" => total_tax = 0
		nota : $item_data[subtotal_tax] è l'aliquota originale del prodotto,
		$item_data['total_tax'] è l'aliquota dopo qualche operazione (i.e.: applicato coupon)
		*/
		if($item_data['subtotal_tax'] == 0 && !$item_data['tax_class']) {
            $vat = 0;
            $vatDescription = 'IVA ' . $vat. '%';
		}
			 
		if($item_data['total'] != 0)
		{
			$stringVatCode = intval($vat);
			$total_tax = $item_data['total_tax'];
				
			if(!array_key_exists($stringVatCode, $taxTotals))
				$taxTotals[$stringVatCode] = $total_tax;
			else
				$taxTotals[$stringVatCode] += $total_tax;
		}
			 
		// gestione sconti
		$discount = $item_data['subtotal'] - $item_data['total']; 
			 
		if($discount > 0)
		{
			$couponApplied = true;
			$stringVatCode = intval($vat); // mi serve intero perché lo uso come indice nell'array
			if(!array_key_exists($stringVatCode, $couponDiscounts))
				$couponDiscounts[$stringVatCode] = $discount;
			else
				$couponDiscounts[$stringVatCode] += $discount;
		}
			 
		// uso il metodo item_tax per cercare l'id dell'aliquota e - se lo trovo - gestire la Natura in modo corretto		  
		foreach($order->get_items( 'tax' ) as $item_id => $item_tax)
		{
			$tax_data = $item_tax->get_data();
			$vatNature = fatt_24_getNatureColumn($tax_data['rate_id']);
		}
		//$shipping_data_name = ''; Davide, commento per futura cancellazione
			 
		// tag xml per riga prodotto		 
		$xml->startElement('Row');
		$xml->writeElement('Description', $item_name);
		$xml->writeElement('Code', $sku);
		$xml->writeElement('Qty', $qty);
		$xml->writeElement('Um', FATT_24_PRODUCT_XML_UM);
		$xml->writeElement('Price', $price);
		$xml->writeElement('VatCode', $vat);
		$xml->writeElement('VatDescription', $vatDescription);
		if (intval($vat) == 0) // con aliquota a zero riporta la natura
			$xml->writeElement('FeVatNature', $vatNature);

		if(!empty($idPdc))
			$xml->writeElement('IdPdc', $idPdc);
	    $xml->endElement(); // Row	
			 
	}
		
	// dati di spedizione
	foreach( $order->get_items( 'shipping' ) as $item_id => $shipping_item_obj ){
				
		$shipping_item_data = $shipping_item_obj->get_data();
		fatt_24_trace ('Dati di spedizione :', $shipping_item_data); // cosa c'è nell'oggetto?
			
		$shipping_data_name         = $shipping_item_data['name'];
		$shipping_data_method_title = $shipping_item_data['method_title'];
		$shipping_data_method_id    = $shipping_item_data['method_id'];
		$shipping_data_instance_id  = $shipping_item_data['instance_id'];
		$shipping_data_total        = $shipping_item_data['total'];
		$shipping_data_total_tax    = $shipping_item_data['total_tax'];
		$shipping_data_taxes        = $shipping_item_data['taxes'];
				
		/**
		 * Qui provo a utilizzare il metodo get_shipping_tax_rates
		 * se il carrello è vuoto mi prendo l'aliquota dai prodotti Davide Iandoli 28.02.2020 - migliorato in data 12.03.2020
		 */
		
		if(!empty(WC()->cart)) { // se il carrello non è vuoto mi prendo le aliquote di woocommerce
			$shipping_tax_rates = \WC_Tax::get_shipping_tax_rates();
						
			if(!empty($shipping_tax_rates))
				$shipping_tax_data = reset($shipping_tax_rates);
						
			$shippingVat = $shipping_tax_data['rate'];
			$shippingDescription = $shipping_tax_data['label'];
			//fatt_24_trace('shipping vat 1 :', $shippingVat);
		} else {
		
		 	$tax_rates = \WC_Tax::get_base_tax_rates($product->get_tax_class(true));
				
			if(!empty($tax_rates))
		   		$tax_rate = reset($tax_rates);
				
			if ($tax_rate['shipping'] == 'yes') {
				$shippingVat = $tax_rate['rate'];
				$shippingDescription = $tax_rate['label'];
			} 
			//fatt_24_trace('shipping vat 2 :', $shippingVat);
		}
				
		//fatt_24_trace(' shipping vat :', $shippingVat); // decommentare per debug su iva spedizione. Non cancellare il codice: consente controlli più rapidi sulle variabili
		//fatt_24_trace(' shipping vat description :', $shippingDescription); // decommentare per debug su iva spedizione
		/**
		* Solo se $shippingVat è vuoto mi calcolo l'aliquota dai dati di spedizione che mi arrivano
		* rimosso $shipping_data_total_tax dalla if - Davide Iandoli 28.02.2020 
		*/
		if (!$shippingVat){ 
			$shippingVat = $fixnum(fatt_24_vat_from_price_and_tax($shipping_data_total, $shipping_data_total_tax), 0);
			$shippingDescription = 'IVA ' . $shippingVat . "%";
		}	
				
		if(isset($tax_data))
			$shipping_vatNature = fatt_24_getNatureColumn($tax_data['rate_id']);
		else
		    $shipping_vatNature = fatt_24_getNatureColumn(); // se non ho il tax_id mi vado comunque a cercare una Natura nella sez. "Configurazione Tassa"
									
		// tag xml per dati di spedizione
		$xml->startElement('Row');
		$xml->writeElement('Description', $shipping_data_name);
		$xml->writeElement('Qty', 1);
		$xml->writeElement('Price', $shipping_data_total);
		$xml->writeElement('VatCode', $shippingVat);
		$xml->writeElement('VatDescription', $shippingDescription);
		if (intval($shippingVat == 0)) // con aliquota a zero riporta la natura
			$xml->writeElement('FeVatNature', $shipping_vatNature);
		$xml->endElement();
	}
		
			// gestione delle commissioni
	foreach ($order->get_items('fee') as $fee_id => $fee_item) {
				   
		$fee_item_data = $fee_item->get_data();
		fatt_24_trace( ' fee item data :', $fee_item_data); // sempre meglio sapere cosa contiene l'oggetto
				
		$fee_item_name = $fee_item_data['name'];
		$fee_item_total = $fee_item_data['total'];
		$fee_item_tax_zero_rate = $fee_item_data['total_tax'] == 0; // restituisce true o false
		
		// gestione aliquote per le commissioni
		$tax_rates = \WC_Tax::get_base_tax_rates($fee_item->get_tax_class(true));
		//fatt_24_trace('fee tax rate :', $tax_rates);			
		if (!empty($tax_rates)) 
			$tax_rate = reset($tax_rates);
			
		$fee_vat = $tax_rate['rate'];
		$fee_vatDescription = $tax_rate['label']; 
							
		// gestione della Natura per le aliquote
		if ($fee_item_tax_zero_rate && !isset($fee_vat)) {
	    	$fee_vat = 0;
			if(!isset($fee_vatDescription))
				$fee_vatDescription = 'IVA ' . $vat . '%';
        }	
			
		$fee_vatNature = fatt_24_getNatureColumn($tax_data['rate_id']);	    
        // tag xml per le commissioni
		$xml->startElement('Row');
		$xml->writeElement('Description', $fee_item_name);
		$xml->writeElement('Qty', 1);
		$xml->writeElement('Price', $fee_item_total);
		$xml->writeElement('VatCode', $fee_vat);
		$xml->writeElement('VatDescription', $fee_vatDescription);
		if (intval($fee_vat) == 0) // con aliquota a zero riporta la natura
			$xml->writeElement('FeVatNature', $fee_vatNature);
		$xml->endElement();
	}
	         		
		
	if($couponApplied == true)
	{
		$vatNature='';
		foreach($order->get_items( 'tax' ) as $item_id => $item_tax)
		{
			$tax_data = $item_tax->get_data();
			$vatNature = fatt_24_getNatureColumn($tax_data['rate_id']);
		}
		
		// se esiste il metodo 'get_coupon_codes' uso quello, altrimenti uso il vecchio per compatibilità con versioni precedenti di WC
		if (method_exists($order, 'get_coupon_codes')) {
			$coupons = $order->get_coupon_codes();
		} else {
			$coupons = $order->get_used_coupons();
		}
			
		$nCoupons = count($coupons);
		if($nCoupons == 1)
			$descrizioneCoupon = 'Coupon ' . $coupons[0];
		else if($nCoupons > 1)
		{
			$descrizioneCoupon = 'Coupons ';
			for($i=0;$i<$nCoupons;$i++)
			{
				$descrizioneCoupon .= $coupons[$i];
				if($i<$nCoupons-1)
				{
					$descrizioneCoupon .= ', ';
				}
			}
		}
		else
			$descrizioneCoupon = 'Sconto';
		/**
		 * I coupon vengono accorpati per aliquota IVA
		 * in caso di aliquote multiple vengono create più righe 
		 * e viene aggiunta alla descrizione 'IVA x%' 
		 */
		foreach ($couponDiscounts as $taxCode => $amount)
		{
			//fatt_24_trace('Coupons: ', $couponDiscounts);
			$price = - $amount;
			$description = $descrizioneCoupon;
			if(count($couponDiscounts) > 1) 
				$description .= ' (Iva ' . $taxCode . '%)'; 
			
			$vatNature = isset($tax_data['rate_id']) ? fatt_24_getNatureColumn($tax_data['rate_id']) : fatt_24_getNatureColumn();
			$vatDescription = $taxNames[$taxCode]; // la descrizione la prendo dall'array
			
			// tag xml per i coupon
			$xml->startElement('Row');
			$xml->writeElement('Description', $description);
			$xml->writeElement('Qty', '1');
			$xml->writeElement('Price', $fixnum($price, 2));
			$xml->writeElement('VatCode', $taxCode);
			if(!empty($vatDescription))
				$xml->writeElement('VatDescription', $vatDescription);
			if($taxCode == 0)
				 $xml->writeElement('FeVatNature', $vatNature);
			$xml->endElement(); // end Row
		}
	}
				   
	$xml->endElement(); // tag xml Rows
	$xml->endElement(); // tag xml Document
	$xml->endElement(); // tag xml Fattura24
	$xml->endDocument();
	
	return $xml->outputMemory(TRUE);
}

// salva contatto in rubrica f24
function fatt_24_SaveCustomer($order_id)
{
	$order = new \WC_Order($order_id);
	fatt_24_trace('fatt_24_SaveCustomer', $order);
	$xml = new \XMLWriter();
	if (!$xml->openMemory())
		throw new \Exception(__('Cannot openMemory', 'fattura24'));
	$xml->startDocument('1.0', 'UTF-8');
	$xml->setIndent(2);
	$xml->startElement('Fattura24');
	$xml->startElement('Document');
	$user = $order->get_user();
	$billing_address = $order->get_formatted_billing_address();
	$shipping_address = $order->get_formatted_shipping_address();
	$Email = $order->get_billing_email();
	if (!$Email)
		if ($user)
			$Email = $user->user_email;
	
	$CellPhone = $order->get_billing_phone();
	$Address    = apply_filters(FATT_24_DOC_ADDRESS, $order);
	$Postcode   = fatt_24_make_strings( array($order->get_billing_postcode()),
   										array($order->get_shipping_postcode()));
	$City       = fatt_24_make_strings( array($order->get_billing_city()),
										array($order->get_shipping_city()));
	$Province   = fatt_24_make_strings( array($order->get_billing_state()),
										array($order->get_shipping_state()));
			
	$Country    = WC()->countries->countries[fatt_24_make_strings(array($order->get_billing_country()),array($order->get_shipping_country()))]; 
			
	$FiscalCode = strtoupper(fatt_24_order_c_fis($order)); // tutto maiuscolo
	$VatCode    = fatt_24_order_p_iva($order);
	$Recipientcode  = fatt_24_order_recipientcode($order);
	$Pecaddress     = fatt_24_order_pecaddress($order);
	
	$Name   = fatt_24_make_strings( array($order->get_billing_company()),
									array($order->get_shipping_company()));
	if (empty($Name)) 
		$Name   = fatt_24_make_strings( array($order->get_billing_first_name(), 
											  $order->get_billing_last_name()),
											  array($order->get_shipping_first_name(), $order->get_shipping_last_name()));
	
    // dati contatto
	$field = function($v, $max) { 
		return substr($v, 0, $max); 
	};
		
	$customerData = array(
		'Name'      => $Name,
		'Address'   => $field($Address, FATT_24_API_FIELD_MAX_indirizzo),
		'Postcode'  => $field($Postcode, FATT_24_API_FIELD_MAX_cap),
		'City'      => $field($City, FATT_24_API_FIELD_MAX_citta),
		'Province'  => $field($Province, FATT_24_API_FIELD_MAX_provincia),
		'Country'   => $field($Country, FATT_24_API_FIELD_MAX_paese),
		'CellPhone' => $CellPhone,
		'FiscalCode'=> $FiscalCode,
		'VatCode'   => $VatCode,
		'Email'     => $Email,
		'FeDestinationCode' => $Recipientcode,
		'PEC' => $Pecaddress,
	);
	$customerData = apply_filters(FATT_24_CUSTOMER_USER_DATA, $customerData);
		
	foreach ($customerData as $k => $v){
		if($k!=="Recipientcode" && $k!=="PEC")
			$xml->writeElement('Customer' . $k, $v);
	}   
		
	$DeliveryName = $order->get_shipping_company();
	if (empty($DeliveryName))
		$DeliveryName = trim($order->get_shipping_first_name() . " " . $order->get_shipping_last_name());
	$DeliveryAddress = trim(trim($order->get_shipping_address_1()) . " " . trim($order->get_shipping_address_2()));
	$DeliveryPostcode = $order->get_shipping_postcode();
	$DeliveryCity = $order->get_shipping_city();
	$DeliveryProvince = $order->get_shipping_state();
	$DeliveryCountry = $order->get_shipping_country();
	
	$customerDeliveryData = array(
		'Name'      => $DeliveryName,
		'Address'   => $DeliveryAddress,
		'Postcode'  => $DeliveryPostcode,
		'City'      => $DeliveryCity,
		'Province'  => $DeliveryProvince,
		'Country'   => $DeliveryCountry,
	);
		
	foreach($customerDeliveryData as $k => $v)
		$xml->writeElement('Delivery'.$k, $v);
	
	$xml->endElement(); // tag xml Document
	$xml->endElement(); // tag xml Fattura24
	$xml->endDocument();
		
	// salvo il contatto solo se non creo alcun tipo di documento
	if(!isset($docType)) {
		$res = fatt_24_api_call('SaveCustomer', array('xml' => $xml->outputMemory(TRUE)));
		$ans = simplexml_load_string($res);
			if (is_object($ans))
			{
	    		fatt_24_order_status_set_doc_data($status, intval($ans->returnCode), strval($ans->description), intval($ans->docId), DT_ANAG);
				$rc = true;
			}
			else
			{
				fatt_24_order_status_set_error($status, sprintf(__('Unknow error occurred while uploading order %d', 'fattura24'), $order_id));
				$rc = false;
			}
		fatt_24_store_order_status($order_id, $status);
		fatt_24_trace('Save Customer rc', $rc, $ans);
	
		return $rc;
	}
}

// lista dei modelli in F24
function fatt_24_getTemplate($isOrder) 
{ 
	$res = fatt_24_api_call('GetTemplate', array());
	$listaNomi = array();
	$listaNomi['Predefinito'] = 'Predefinito';
	$xml = simplexml_load_string(utf8_encode($res));
	if (is_object($xml))
	{
		$listaModelli = $isOrder ?  $xml->modelloOrdine : $xml->modelloFattura;
		foreach($listaModelli as $modello)
			$listaNomi[intval($modello->id)] = str_replace('\'', '\\\'', strval($modello->descrizione)) . " (ID: " . intval($modello->id) . ")";
	}
	else
		fatt_24_trace('error list templates', $res);
	return $listaNomi;
}

// lista Pdc in F24
function fatt_24_getPdc()
{
	$res = fatt_24_api_call('GetPdc', array());
	$listaNomi = array();
	$listaNomi['Nessun Pdc'] = 'Nessun Pdc';
	$xml = simplexml_load_string(utf8_encode($res));
	if (is_object($xml))
	{
		foreach($xml->pdc as $pdc)
			if(intval($pdc->ultimoLivello) == 1)
				$listaNomi[intval($pdc->id)] = str_replace('^', '.', strval($pdc->codice)) . 
					' - ' . str_replace('\'', '\\\'', strval($pdc->descrizione)); // visualizza la descrizione e non l'id per migliore esperienza d'uso
	}
	else
		fatt_24_trace('error list pdc', $res);
	return $listaNomi;
}

// lista sezionali in F24
function fatt_24_getSezionale($idTipoDocumento)
{
	$res = fatt_24_api_call('GetNumerator', array());
	$listaNomi = array();
	$listaNomi['Predefinito'] = 'Predefinito';
	$xml = simplexml_load_string(utf8_encode($res));
	if (is_object($xml))
	{
		foreach($xml->sezionale as $sezionale)
			foreach($sezionale->doc as $doc)
				if(intval($doc->id) == $idTipoDocumento && intval($doc->stato) == 1)
				  $listaNomi[intval($sezionale->id)] = strval($sezionale->anteprima); // visualizza l'anteprima e non l'id per migliore esperienza d'uso 
	}
	else
		fatt_24_trace('error list sezionale', $res);
	return $listaNomi;
}

/**
* Gestione Natura IVA: il @param $tax_id assume valore di default = null per gestire i casi
* in cui non riesco ad ottenerne il valore (es.: aliquota spedizione != aliquota prodotto)
*/
function fatt_24_getNatureColumn($tax_id = null)
{
	global $wpdb;
	$table_name = $wpdb->prefix . 'fattura_tax';
	$sql = "SELECT tax_code FROM $table_name"; // prima query per tax_code, vuoto solo se non impostato nel plugin
	$mycodeboj = $wpdb->get_row($sql);
	if(isset($tax_id)) { // query diversa se $tax_id != null
		$sql2 = "SELECT tax_code FROM $table_name WHERE tax_id =".$tax_id .""; // dove possibile uso gli elementi dell'ordine
		$mycodeboj2 = $wpdb->get_row( $sql2 ); 
	}
		
	if(isset($mycodeboj2))
	   $result_object = $mycodeboj2; // se l'oggetto $mycodeboj2 non è vuoto gestisco questo
	else
	   $result_object = $mycodeboj;

	$array['tax_code'] = ''; // la query dà come risultato una stdClass, il codice da qui
	if (is_object($result_object)) // è stato aggiunto per gestirla correttamente
		$array = get_object_vars($result_object);
	$tax_code = $array['tax_code']; // Qui ottengo il risultato della colonna Natura, anche se non ho il tax_id
	return $tax_code; 
}