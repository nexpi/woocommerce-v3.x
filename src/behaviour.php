<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * gestisce i metodi e il comportamento del plugin
 * 
 */
namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'uty.php';
require_once 'constants.php';

if (!is_admin())
    require_once ABSPATH.'wp-admin/includes/file.php';

// filtro che restituisce  il file del documento in formato PDF	
add_filter(FATT_24_DOC_PDF_FILENAME, function($args) {
    /**
	* $doc_ajax_param è la variabile con cui passo al comando ajax f24_pdfcmd.js
	* il tipo di documento selezionato dall'utente, anche se fosse FE - Davide Iandoli 28.06.2019
 	*/
	$doc_ajax_param = $args['doc_ajax_param'];
    $order_id = $args['order_id'];
    $timestamp = fatt_24_now('YmdHis');
    return sprintf('doc-%s-%s-%s.pdf', $timestamp, $doc_ajax_param, $order_id);
});

// prendo la p. iva dai metadati dell'ordine, tramite il campo personalizzato
add_filter(FATT_24_ORDER_GET_VAT, function($order) {
    return get_post_meta($order->get_id(), '_billing_vatcode', true);
});

// la restituisco per assegnarla a una variabile e codificarla in xml
function fatt_24_order_p_iva($order) {
	return apply_filters(FATT_24_ORDER_GET_VAT, $order);
}

// da qui in poi il codice segue la stessa logica delle righe 32 e seguenti per tutti gli altri campi fiscali
add_filter(FATT_24_ORDER_GET_CF, function($order) {
    return get_post_meta($order->get_id(), '_billing_fiscalcode', true);
});

function fatt_24_order_c_fis($order) {
	return apply_filters(FATT_24_ORDER_GET_CF, $order);
}

function fatt_24_customer_use_vat() {
	return apply_filters(FATT_24_CUSTOMER_USE_VAT, null);
}

add_filter(FATT_24_CUSTOMER_USE_VAT, function() { 
	return true; 
});

function fatt_24_customer_use_cf() { 
	return apply_filters(FATT_24_CUSTOMER_USE_CF, null);
}

add_filter(FATT_24_CUSTOMER_USE_CF, function() { 
	return true; 
});

add_filter(FATT_24_ORDER_GET_PEC_ADDRESS, function($order) {
   return get_post_meta($order->get_id(), '_billing_pecaddress', true);
});

function fatt_24_order_pec_address($order) { 
	return apply_filters(FATT_24_ORDER_GET_PEC_ADDRESS, $order);
}

add_filter(FATT_24_ORDER_GET_RECIPIENTCODE, function($order) {
	return get_post_meta($order->get_id(), '_billing_recipientcode', true);
});

function fatt_24_order_recipientcode($order) {
	return apply_filters(FATT_24_ORDER_GET_RECIPIENTCODE, $order);
}

function fatt_24_customer_use_recipientcode() {
	return apply_filters(FATT_24_CUSTOMER_USE_RECIPIENTCODE, null);
}

add_filter(FATT_24_CUSTOMER_USE_RECIPIENTCODE, function() { 
	return true; 
});

function fatt_24_order_pecaddress($order) {
	return apply_filters(FATT_24_ORDER_GET_PEC_ADDRESS, $order);
}

function fatt_24_customer_use_pecaddress() {
	return apply_filters(FATT_24_CUSTOMER_USE_PEC_ADDRESS, null);
}

add_filter(FATT_24_CUSTOMER_USE_PEC_ADDRESS, function() { 
	return true; 
});

// con questo metodo ottengo l'indirizzo dal nome del file
function fatt_24_get_url_from_file($file) {
	$hp = get_home_path();
	$last_hp_ = explode('/',substr($hp,0,-1));
	$last_hp = end($last_hp_);
	$last_file_ = explode($last_hp,$file);
	$last_file = end($last_file_);
	
	return home_url().$last_file;
}

/**
* $doc_ajax_param è la variabile con cui passo al comando ajax f24_pdfcmd.js
* il tipo di documento selezionato dall'utente - Davide Iandoli 28.06.2019
*/
// con questo metodo ottengo l'indirizzo dal nome del file PDF
function fatt_24_PDF_filename($doc_ajax_param, $order_id) {
	return apply_filters(FATT_24_DOC_PDF_FILENAME, compact('doc_ajax_param', 'order_id'));
}

// imposto i permessi del file
function fatt_24_set_file_permissions($new_file) {
	$stat = @stat(dirname($new_file));
	$perms = $stat['mode'] & 0007777;
	$perms = $perms & 0000666;
	@chmod($new_file, $perms);
}

// salva il file, @param $status, $order_id, $PDF
add_filter(FATT_24_DOC_STORE_FILE, function($status, $order_id, $PDF) {

	/**
	 * con $docParam passo alla funzione fatt_24_peek
	 * il valore scelto dall'utente nelle impostazioni - nella sezione "Crea fattura"
	 * Davide Iandoli 28.06.2019
	 */
    $fatt_inv_create_new = get_option("fatt-24-inv-create");
	
	if ($fatt_inv_create_new == 1) {
		$docParam = FATT_24_DT_FATTURA;
	} else if ($fatt_inv_create_new == 2) {
    	$docParam = FATT_24_DT_FATTURA_ELETTRONICA;
	}

	$docType = fatt_24_peek($status, 'docType', $docParam);	// mi lo status il docType e $docParam
    $oldPdfPath = fatt_24_peek($status, 'pdfPath');
    $file = fatt_24_PDF_filename($docType, $order_id);
    $folder = trailingslashit(FATT_24_DOCS_FOLDER); // la cartella è definita nelle costanti
    $wpdir = wp_upload_dir();
    $basedir = $wpdir['basedir'];
    $dir = $basedir.'/'.$folder;
    wp_mkdir_p($dir);
    $new_file = $dir.$file;
    if (!file_exists($dir . 'index.php'))
        file_put_contents($dir . 'index.php', '<?php');

    $ifp = @fopen($new_file, 'wb');
    if (!$ifp)
        fatt_24_order_status_set_error($status, sprintf(__('Could not write file %s', 'fattura24'), $new_file));
    else {
        @fwrite($ifp, $PDF);
        fclose($ifp);
        fatt_24_set_file_permissions($new_file);
        fatt_24_order_status_set_file_data($status, $new_file, $docType);
        if ($oldPdfPath && is_file($oldPdfPath))
            unlink($oldPdfPath);
    }
    return $status;
}, 10, 3);

// Note a piè pagina della fattura
function fatt_24_doc_footnotes($order) {
	$FootNotes = sprintf(__('order num. %d', 'fattura24'), $order->get_id());
		if ($order->get_customer_note())
	return $FootNotes;
}

add_filter(FATT_24_DOC_FOOTNOTES, function($order) {
    return fatt_24_doc_footnotes($order);
});

/* commentata per inutilizzo e futura cancellazione Davide Iandoli 11.02.2020
// codice prodotto (verificare la possibilità di riutilizzarlo sostituendolo al metodo attuale)
add_filter(FATT_24_DOC_PRODUCT_CODE, function($item) {
	$product = $item->get_product();
    return $product->get_sku();
});*/

// indirizzi
add_filter(FATT_24_DOC_ADDRESS, function($order) {
    return fatt_24_make_strings(    array($order->get_billing_address_1(), $order->get_billing_address_2()),
           array($order->get_shipping_address_1(), $order->get_shipping_address_2()));
});

// widget schermata impostazioni
add_filter(FATT_24_LAYOUT_OPTION, function($args) {
    extract($args);
    $rc = $widget;
    if ($help) $rc .= fatt_24_helpico($help);
    if ($desc) $rc .= $desc;
    return $rc;
});

// sposta documenti pdf
function fatt_24_move_docs($folder) {
	$folder = trailingslashit($folder);
	$wpdir = wp_upload_dir();
	$basedir = $wpdir['basedir'];
	$dir = $basedir.'/'.$folder;
	wp_mkdir_p($dir);
	
	$recs_postmeta = 0;
	$ok_meta_value = 0;
	$ko_meta_value = 0;
	$has_pdfPath = 0;
	$no_pdfPath = 0;
	$moved = 0;
	$cant_move = 0;

	global $wpdb;
	foreach($wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key=%s", FATT_24_ORDER_INVOICE_STATUS)) as $r) {
		++$recs_postmeta;
			if ($i = maybe_unserialize($r->meta_value)) {
				++$ok_meta_value;
				if (isset($i['pdfPath']) && is_file($i['pdfPath'])) {
					++$has_pdfPath;
					$old = $i['pdfPath'];
					$file = basename($old);
					$new = $dir.$file;
	
					if ($new != $old && rename($old, $new)) {
						fatt_24_set_file_permissions($new);
						++$moved;
						$i['pdfPath'] = $new;
						fatt_24_store_order_status($r->post_id, $i);
					}
				else
					++$cant_move;
				}
				else
					++$no_pdfPath;
			}
			else
				++$ko_meta_value;
	}
	return compact(
		'recs_postmeta',
		'ok_meta_value',
		'ko_meta_value',
		'has_pdfPath',
		'no_pdfPath',
	    'moved',
		'cant_move'
	);
}
