<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * aggancia gli hooks di WooCommerce
 * 
 */
namespace fattura24;


if (!defined('ABSPATH')) exit;

require_once 'api_call.php';
require_once 'uty.php';
require_once 'order_status.php';

/**
 * Con questa funzione controllo se il paese è IT e gestisco il flag nelle impostazioni. 
 * Per tutti gli altri paesi il valore è sempre false (e il campo è nascosto) - Davide Iandoli 20.02.2020
 */


function fatt_24_CF_flag(){
    global $woocommerce;
		
	if (method_exists($woocommerce->customer, 'get_billing_country')) {
		$country = $woocommerce->customer->get_billing_country();
	} else {
		$country = $woocommerce->customer->get_country(); // deprecato da WooCommerce 3.0
    }
    
    if ($country == 'IT') {
        $result = get_option("fatt-24-abk-fiscode-req") == 1 ? true : false;
        return $result;
    } else {
        return false;
    }
}
/**
 * recupero l'ultimo elemento di un array senza spostare il puntatore
 */
function fatt_24_array_key_last($array) {
    $key = NULL;
    if ( is_array( $array ) ) {
        end( $array );
        $key = key( $array );
    }
    return $key;
}

// gestisco le azioni dell'ordine => utilizzato per le azioni di creazione PDF ordine e fattura
function fatt_24_order_action($order_id) {
    
    $order = new \WC_Order($order_id);
    $transient_id = 'order_action-'.$order_id;

    if (get_transient($transient_id) === false)
	{
   		set_transient($transient_id, true, 60);
           
  	/* non crea l'ordine se il totale dell'ordine è zero - Davide 20.01.2019 */
	if ($order->get_total() + $order->get_total_tax() != 0) {
		fatt_24_process_order($order_id);
	}
      	fatt_24_process_customer($order_id);
		delete_transient($transient_id);
	}
	else
        fatt_24_trace('transient blocked', $transient_id);
}

// convalida l'ordine controllando i meta dati dell'utente 
function fatt_24_validate_order($order_id){ 
    $order = new \WC_Order($order_id);
	$dati_utente = get_user_meta($order->get_user_id());
	if($dati_utente)
		return true;
	return false;
}

// controlla se un ordine risulta processato nel checkout Wc
add_action('woocommerce_checkout_order_processed', function($order_id) {
    fatt_24_order_action($order_id);
});

// creazione ordine da admin
add_action('wp_insert_post', function($order_id) {
    if(!did_action('woocommerce_checkout_order_processed') && (get_post_type($order_id) == 'shop_order' || get_post_type($order_id) == 'shop_subscription') && fatt_24_validate_order($order_id))
        fatt_24_order_action($order_id);
});

// con questa variabile consento di scegliere lo status dell'ordine per creare una fattura
$fatt_24_hook = get_option('fatt-24-ord-status-select');  //Qui mi prendo le impostazioni dal menu a discesa

if($fatt_24_hook == 0) {
   $fatt_24_action = 'woocommerce_order_status_completed'; // Ordine in stato completato
} else {
   $fatt_24_action = 'woocommerce_order_status_processing'; // Ordine in lavorazione
}    

add_action($fatt_24_action, function($order_id) { // si attiva l'hook in base alla selezione - Davide Iandoli 03.06.2019
    $order = new \WC_Order($order_id);
    if ($order->get_total() + $order->get_total_tax() != 0) { // non crea ordine con totale 0 in F24
        fatt_24_process_fattura($order_id);
    } 
    /**
    * Con $docParam controllo se il tipo di documento è FATT_24_DT_FATTURA oppure FATT_24_DT_FATTURA_ELETTRONICA
    * e passo il parametro alla funzione di controllo fatt_24_is_PDF_available - Davide Iandoli 28.06.2019
    */
    $fatt_inv_create_new = get_option("fatt-24-inv-create");
	if ($fatt_inv_create_new == 1) {
    	$docParam = FATT_24_DT_FATTURA;
    } else if ($fatt_inv_create_new == 2) {
   		$docParam = FATT_24_DT_FATTURA_ELETTRONICA;
    }
    if(!is_null($docParam))
        if(!fatt_24_is_PDF_available($order_id, $docParam)) 
            fatt_24_download_PDF($order_id); // scarico il PDF del documento se è disponibile
});

// aggiungo i campi fiscali al checkout dell'ordine, e li inserisco in fondo grazie all'attributo priority
add_action('woocommerce_checkout_fields', function($fields) {
    $fields['billing']['billing_fiscalcode'] = array(
        'type'       => 'text',
        'placeholder' => __('Codice fiscale', 'fattura24'),
        'label'      => __('Codice fiscale', 'fattura24'), 
        'required'   => fatt_24_CF_flag(), // controlla se è presente la spunta solo se il paese è IT
        'class'      => array('form-row-wide', 'inv_create_elecinvoice'), // lo aggiungo ai campi nascosti se il paese != IT
        'priority'   => 120,
        'clear'      => true
     );

     $fields['billing']['billing_vatcode'] = array(
        'type'       => 'text',
        'placeholder' => __('Partita Iva / VAT', 'fattura24'),
        'label'      => __('Partita Iva / VAT', 'fattura24'),
        'required'   => fatt_24_get_flag(FATT_24_ABK_VATCODE_REQ),
        'class'      => array('form-row-wide'),
        'priority'   => 120,
        'clear'      => true,
     );

     $fields['billing']['billing_recipientcode'] = array(
        'type' => 'text',
        'placeholder' => __('Codice Destinatario', 'fattura24'),
        'label' => __('Codice Destinatario', 'fattura24'),
        'maxlength' => 7, // definisco la lunghezza massima dell'input
        'class' => array('form-row-wide','inv_create_elecinvoice'),
        'priority' => 120, // cambio l'ordine dei campi nel layout
        'clear' => true
     );

     $fields['billing']['billing_pecaddress'] = array(
         'type' => 'email',
         'placeholder' => __('Indirizzo PEC', 'fattura24'),
         'label' => __('Indirizzo PEC', 'fattura24'),
         'validate' => array('email'),
         'class' => array('form-row-wide','inv_create_elecinvoice'), 
         'priority' => 120,
         'clear' => true
     );
    return $fields;
});

// con questo hook aggiungo i campi personalizzati nell'ordine woocommerce attraverso il metodo update_post_meta
add_action('woocommerce_checkout_update_order_meta', function($order_id) {
    if (!empty(sanitize_text_field($_POST['billing_fiscalcode']))) {
        update_post_meta($order_id, '_billing_fiscalcode', sanitize_text_field(strtoupper($_POST['billing_fiscalcode'])));

    }
    if (!empty(sanitize_text_field($_POST['billing_vatcode']))) {
       update_post_meta($order_id, '_billing_vatcode', sanitize_text_field($_POST['billing_vatcode']));
    }
    if (!empty(sanitize_text_field($_POST['billing_pecaddress']))) {
        update_post_meta($order_id, '_billing_pecaddress', sanitize_text_field($_POST['billing_pecaddress']));
    }
    if (!empty(sanitize_text_field($_POST['billing_recipientcode']))) {
        update_post_meta($order_id, '_billing_recipientcode', sanitize_text_field($_POST['billing_recipientcode']));
    }
});

// con questo hook registro i dati dei campi fiscali nell'account utente woocommerce, attraverso il metodo update_user_meta
add_action('woocommerce_created_customer', function($customer_id) {
    if (isset($_POST['billing_fiscalcode'])) {
        update_user_meta($customer_id, 'billing_fiscalcode', sanitize_text_field(strtoupper($_POST['billing_fiscalcode'])));
    }
    if (isset($_POST['billing_vatcode'])) {
        update_user_meta($customer_id, 'billing_vatcode', sanitize_text_field($_POST['billing_vatcode']));
    }
    if (isset($_POST['billing_recipientcode'])) {
        update_user_meta($customer_id, 'billing_recipientcode', sanitize_text_field($_POST['billing_recipientcode']));
    }
    if (isset($_POST['billing_pecaddress'])) {
        update_user_meta($customer_id, 'billing_pecaddress', sanitize_text_field($_POST['billing_pecaddress']));
    }
});

//con questo hook aggiungo gestico i campi fiscali aggiuntivi usando i metadati del cliente
add_filter('woocommerce_customer_meta_fields', function($fields) {
    if (fatt_24_customer_use_cf() || fatt_24_customer_use_vat()) {
        if (!isset($fields['fatt_24']))
            $fields['fatt_24'] = array(
                'title' => __('Invoicing required fields', 'fattura24'),
                'fields' => array()
            );
        if (fatt_24_customer_use_cf())
            $fields['fatt_24']['fields']['billing_fiscalcode'] = array(
                'label' => __('Fiscal Code', 'fattura24'),
                'description' => __('a valid Fiscal Code', 'fattura24'),
            );
        if (fatt_24_customer_use_vat())
            $fields['fatt_24']['fields']['billing_vatcode'] = array(
                'label' => __('Vat code', 'fattura24'),
                'description' => __('a valid Vat Code', 'fattura24'),
            );
    }

    if (fatt_24_customer_use_recipientcode()) {
        $fields['fatt_24']['fields']['billing_recipientcode'] = array(
            'label' => __('Recipient Code', 'fattura24'),
            'description' => __('a valid Recipient Code', 'fattura24'),
        );

        $fields['fatt_24']['fields']['billing_pecaddress'] = array(
            'label' => __('Indirizzo PEC', 'fattura24'),
            'description' => __('a valid PEC address', 'fattura24'),
        );
    }
    return $fields;
});

// con questi metodi gestisco l'aggiornamento dei dati fiscali nell'account utente e ne consento il salvataggio
function fatt_24_user_vatcode($user_id) {
	if(empty($user_id)) {
	   	   return ""; } else {
    return get_the_author_meta('billing_vatcode', $user_id); 
   }
}

function fatt_24_user_fiscalcode($user_id) {
	if(empty($user_id))
		return "";
	return get_the_author_meta('billing_fiscalcode', $user_id);
}

function fatt_24_user_recipientcode($user_id) {
	if(empty($user_id))
    	return "";
	return get_the_author_meta('billing_recipientcode', $user_id);
}

function fatt_24_user_pecaddress($user_id) {
	if(empty($user_id))
		return "";
	return get_the_author_meta('billing_pecaddress', $user_id);
}

// con questo hook aggiungo i campi dei dati fiscali al form di registrazione
add_action('woocommerce_edit_account_form', function() {

    if (fatt_24_customer_use_cf() || fatt_24_customer_use_vat()) {
        $user_id = get_current_user_id();
        $user = get_userdata($user_id);
        ?>

        <fieldset>
            <legend>
                <?php _e('Dati fiscali', 'fattura24') ?> <!-- edited label Davide Iandoli 29.01.2019 -->
            </legend>
            <?php if (fatt_24_customer_use_cf()) { ?>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide"> <!-- edited style -->
                    <label for="billing_fiscalcode">
                        <?php __('Codice Fiscale', 'fattura24') ?>
                    </label>

                    <input type="text" class="woocommerce-Input input-text"
                           name="billing_fiscalcode" id="billing_fiscalcode"
                           value="<?php echo esc_attr(strtoupper(fatt_24_user_fiscalcode($user_id))) ?>" />

                    <span class="description">
                        <?php _e('inserisci un codice fiscale valido', 'fattura24') ?> <!-- edited label Davide Iandoli 29.01.2019 -->
                    </span> 
                </p>
            <?php } ?>
     
            <?php if (fatt_24_customer_use_vat()) { ?>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <label for="billing_vatcode">
                        <?php _e('Partita Iva', 'fattura24')?>
                    </label>

                    <input type="text" class="woocommerce-Input input-text"
                           name="billing_vatcode" id="billing_vatcode"
                           value="<?php echo esc_attr(fatt_24_user_vatcode($user_id)) ?>" />

                    <span class="description">
                        <?php _e('inserisci una Partita Iva valida', 'fattura24') ?> <!-- edited label Davide Iandoli 29.01.2019 -->
                    </span> 
                </p>
              <?php } ?> 
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <label for="billing_recipientcode">
                        <?php _e('Codice Destinatario', 'fattura24')?>
                    </label>

                    <input type="text" class="woocommerce-Input input-text"
                           name="billing_recipientcode" id="billing_recipientcode"
                           maxlength="7"
                           value="<?php echo esc_attr(fatt_24_user_recipientcode($user_id)) ?>" />

                    <span class="description">
                        <?php _e('inserire un codice destinatario valido') ?> 
                        <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    
                    <label for="billing_pec">
                        <?php _e('Indirizzo PEC', 'fattura24')?>
                    </label>
        
                    <input type="email" class="woocommerce-Input input-text"
                           name="billing_pecaddress" id="billing_pecaddress"
                           value="<?php echo esc_attr(fatt_24_user_pecaddress($user_id)) ?>" />
           
                    <span class="description">
                        <?php _e('inserire un indirizzo PEC valido', 'fattura24'); ?> 
        </fieldset> 

    <div class="clear"></div>
<?php
    }
});

// salvo i campi dei dati fiscali nell'account utente
add_action('woocommerce_save_account_details', function($user_id) {
    if (fatt_24_customer_use_cf())
        update_user_meta($user_id, 'billing_fiscalcode', htmlentities(sanitize_text_field(strtoupper($_POST['billing_fiscalcode']))));

    if (fatt_24_customer_use_vat())
        update_user_meta($user_id, 'billing_vatcode', htmlentities(sanitize_text_field($_POST['billing_vatcode'])));

    if (fatt_24_customer_use_recipientcode()){
        update_user_meta($user_id, 'billing_recipientcode', htmlentities(sanitize_text_field($_POST['billing_recipientcode'])));
        update_user_meta($user_id, 'billing_pecaddress', htmlentities(sanitize_text_field($_POST['billing_pecaddress'])));
    }
});

// con questo hook rendo i campi aggiuntivi visibili nell'ordine woocommerce
add_action('woocommerce_admin_order_data_after_billing_address', function($order) { ?>
	
    <div class="address">
		<?php
        if (fatt_24_customer_use_cf()){
            echo '<p><strong>' . __('Fiscal Code', 'fattura24') . ':</strong>' .fatt_24_order_c_fis($order). '</p>';
        }
        if (fatt_24_customer_use_vat()){
            echo '<p><strong>' .__('VAT code', 'fattura24'). ':</strong>'  .fatt_24_order_p_iva($order). '</p>';
        }
        if(fatt_24_customer_use_recipientcode()) {
            echo '<p><strong>' . __('Recipient Code', 'fattura24') . ':</strong>' . fatt_24_order_recipientcode($order).'</p>';
            echo '<p><strong>' . __('PEC address', 'fattura24') . ':</strong>'  .fatt_24_order_pec_address($order). '</p>';
        }
        ?>
	</div>
    <div class="edit_address">
    <?php 
        // con questo codice li rendo editabili nell'ordine lato admin usando il metodo woocommerce_wp_input
        if (fatt_24_customer_use_cf()){
		    woocommerce_wp_text_input( array( 'id' => '_billing_fiscalcode', 'label' =>__('Fiscal Code', 'fattura24'), 'wrapper_class' => '_billing_company_field' ) );
        }
        if (fatt_24_customer_use_vat()){
		    woocommerce_wp_text_input( array( 'id' => '_billing_vatcode', 'label' => __('Vat Code', 'fattura24'), 'wrapper_class' => '_billing_company_field' ) );
        }
        if(fatt_24_customer_use_recipientcode()) {
			woocommerce_wp_text_input( array( 'id' => '_billing_recipientcode', 'label' => __('Recipient Code', 'fattura24'), 'wrapper_class' => '_billing_company_field' ) );
			woocommerce_wp_text_input( array( 'id' => '_billing_pecaddress', 'label' => __('PEC Address', 'fattura24'), 'wrapper_class' => '_billing_company_field' ) );
        }?>
    </div>
    <?php
}, 10, 1);

// aggiorno il contenuto dei campi fiscali nel dettaglio dell'ordine
add_action( 'woocommerce_process_shop_order_meta',  function ( $post_id, $post ){
	update_post_meta( $post_id, '_billing_pecaddress', wc_clean( sanitize_text_field($_POST[ '_billing_pecaddress' ] )) );
	update_post_meta( $post_id, '_billing_recipientcode', wc_clean(sanitize_text_field( $_POST[ '_billing_recipientcode' ])));
	update_post_meta( $post_id, '_billing_fiscalcode', wc_clean( sanitize_text_field(strtoupper($_POST[ '_billing_fiscalcode' ]))));
	update_post_meta( $post_id, '_billing_vatcode', wc_clean( sanitize_text_field($_POST[ '_billing_vatcode' ]) ) );
}, 45, 2 );

add_action('woocommerce_admin_order_data_after_order_details', function($order) {
    /**
     * Implemento le azioni di aggiornamento e visualizzazione PDF nel dettaglio ordine
     * utilizzando le variabili $docParam
     */
    $docParam = '';
    $fatt_inv_create_new = get_option("fatt-24-inv-create");
	
	if ($fatt_inv_create_new == 1) {
		$docParam = FATT_24_DT_FATTURA;
	} else if ($fatt_inv_create_new == 2) {
        $docParam = FATT_24_DT_FATTURA_ELETTRONICA;
        
    }

    /**
     * controllo se è già stata creata una fattura, in caso positivo ne riporto il numero
     * nel dettaglio dell'ordine - Davide Iandoli 25.03.2020 - inizio codice
     */ 
    $order_id = $order->get_id();
    $invResult = fatt_24_is_available_on_f24($order_id, $docParam);
    $boxElement = array(fatt_24_h3(__('Fattura24', 'fattura24')), fatt_24_p(fatt_24_actions_of_order($order->get_id(), $docParam))); // spostato il 25.03.2020
    
    if($invResult){
        $invDocNumber = $invResult['docNumber'];
        $message = !empty($invDocNumber)? __('Fattura24 doc n. ', 'fattura24') . $invDocNumber : '';
        $boxElement[] = $message; // aggiungo il messaggio (alla fine dell'array) solo se è stata creata la fattura
    }
    // fine codice aggiunto in questo metodo il 25.03.2020     
        
    if ($fatt_inv_create_new != 0) // disable Fattura24 custom fields if invoice creation is disabled    
        echo fatt_24_div(fatt_24_klass('form-field form-field-wide'), $boxElement); // codice modificato il 25.03.2020
}, 10, 1);

// con questo hook aggiungo le colonne personalizzate di F24 nel dettaglio dell'ordine
add_filter('manage_edit-shop_order_columns', function($columns) {
     /**
     * Aggiungo due nuove colonne per controllare da admin (nel dettaglio ordine)
     * se le chiamate per gli ordini e per le fatture hanno avuto esito positivo
     */
    $new_columns = is_array($columns) ? $columns : array();
    $fatt_inv_create_new = get_option("fatt-24-inv-create"); // opzione scelta per la creazione fattura
    $fatt_24_order_enable = fatt_24_get_flag(FATT_24_ORD_CREATE); // valore checkbox crea ordine
    
    // visualizzo le colonne Fattura24 solo se la creazione delle fatture è abilitata
    if ($fatt_inv_create_new != 0) { 
       $new_columns['fatt24-invoice'] = __('Fattura24', 'fattura24');
       
       if ($fatt_24_order_enable == 1)
            $new_columns['fatt24-order-status'] = __('F24 Order', 'fattura24'); // colonna stato ordini, invisibile se la casella non è contrassegnata da spunta
       $new_columns['fatt24-invoice-status'] = __('F24 Invoice', 'fattura24'); // colonna stato fattura

    }
    return $new_columns;
});

add_action('manage_shop_order_posts_custom_column', function($column) {
    $order = wc_get_order(); // dati dell'ordine WooCommerce
    $order_id = $order->get_id();
    $woo_order_status = $order->get_status(); // stato dell'ordine in Woocommerce

    /**
    * con $docParam passo alla funzione fatt_24_actions_of_order
    * il tipo di documento selezionato dall'utente nelle impostazioni
    * Davide Iandoli 28.06.2019
    */
    $fatt_inv_create_new = get_option("fatt-24-inv-create");
    if ($fatt_inv_create_new == 1) {
	    $docParam = FATT_24_DT_FATTURA;
    } else if ($fatt_inv_create_new == 2) {
        $docParam = FATT_24_DT_FATTURA_ELETTRONICA;
    }
    
    $order_status = fatt_24_get_order_status($order_id); // aggiungo controllo sullo stato dell'ordine
    if ($column == 'fatt24-invoice') {
        echo fatt_24_actions_of_order($order->get_id(), $docParam); // qui chiamo le azioni Create Invoice e derivate (vedi order_status.php)
    } 
    // colonna stato ordine in F24
    if($column == 'fatt24-order-status') {
        /**
        * Con $OrderResult controllo lo stato dell'ordine prendendomi il docId
        * La funzione fatt_24_is_available_on_f24 usa i metodi fatt_24_get_order_status
        * e get_post_meta per recuperare le info, perciò non fa chiamate API
        */ 
        $orderResult = fatt_24_is_available_on_f24($order_id, FATT_24_DT_ORDINE); 
        $OrderDocId = $orderResult['docId']; // docId per l'ordine

        // se il totale dell'ordine è 0 nessun documento è stato creato, perciò non devo vedere il pulsante
        if($order->get_total() + $order->get_total_tax() == 0) { 
        // controllo se in f24 l'ordine è stato già creato e se esiste $order_status['description']   
        } elseif ($OrderDocId || $woo_order_status == 'cancelled' || isset($order_status['description']) && strpos($order_status['description'], 'already exists') !== false ) {
            
        } elseif (!$OrderDocId || isset($order_status['description']) && !strpos($order_status['description'], 'already exists') !== false) {
            echo '<mark style="margin-bottom:2px;" class="order-status status-on-hold tips" data-tip="' . __('Document not created in Fattura24', 'fattura24') . '"><span>'. __('Not saved ', 'fattura24') . '</span></mark><br />';
            echo fatt_24_new_columns_action($order_id, FATT_24_DT_ORDINE); // aggiunge il pulsante solo su questa colonna
        }
    }
    // colonna stato fattura in F24
    if($column == 'fatt24-invoice-status') {
              
        $invResult = fatt_24_is_available_on_f24($order_id, $docParam); //ottengo un array con docId e docNumber
        $invDocId = $invResult['docId']; // docId per la fattura
        $invDocNumber = $invResult['docNumber']; // riporto anche il docNumber
        
        // se il totale dell'ordine è zero lascio il campo vuoto            
        if($order->get_total() + $order->get_total_tax() == 0) {  
        
        // se esistono entrambe le chiavi ho creato anche la fattura    
        } elseif ($invDocId || $woo_order_status == 'cancelled'  || isset($order_status['history']) && $order_status['history']['description'] == 'Operation completed') { // per le fatture mi serve anche il controllo sulla chiave 'history'
            if(!empty($invDocNumber))
                echo __('Fattura24 doc n. ', 'fattura24') . $invDocNumber; // se la fattura è stata creata riporto il riferimento nella colonna
        } elseif (!$invDocId)  {
            echo '<mark style="margin-bottom:2px;" class="order-status status-on-hold tips" data-tip="' . __('Document not created in Fattura24', 'fattura24') . '"><span>'. __('Not saved ', 'fattura24') . '</span></mark><br />';
            echo fatt_24_new_columns_action($order_id, $docParam); // aggiunge il pulsante solo su questa colonna
        }
    }
}, 2);

// con questo filtro gestisco l'azione "visualizza PDF"
 add_filter('woocommerce_my_account_my_orders_actions', function($actions, $order) {
    if ($s = fatt_24_order_status($order->get_id()))
       if ($s['status'] == FATT_24_INVSTA_PDF_AVAIL_LOCAL)
            $actions['pdfView'] = array(
                'url' => fatt_24_order_PDF_url($order->get_id()),
                'name' => __('PDF Doc', 'fattura24'),
            );
    return $actions;
}, 10, 2);

add_action( 'woocommerce_after_checkout_form', function() {
?>

  <script>
  // Con questo script nascondo i campi fiscali (tranne p. iva) se il paese di fatturazione non è IT
    jQuery(document).ready(function($){
        setTimeout(function () {
          $('select#billing_country').trigger('change');
        });

        $('select#billing_country').change(function(){
            if( $(this).val() == 'IT'){
                $('.inv_create_elecinvoice').show();
            } else {
                $('.inv_create_elecinvoice').hide();
            }
        });
    });
  </script>
<?php 
});