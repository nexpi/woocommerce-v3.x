<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * gestisce gli stati dell'ordine in WooCommerce
 * 
 */
namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'constants.php';
require_once 'uty.php';

// Elenco degli stati dell'ordine in WC, non cancellare: può tornare utile per futuri sviluppi
/* from https://docs.woocommerce.com/document/managing-orders/ 

    Pending payment – Order received (unpaid)
    Failed – Payment failed or was declined (unpaid). Note that this status may not show immediately and instead show as pending until verified (i.e., PayPal)
    Processing – Payment received and stock has been reduced- the order is awaiting fulfillment.
    Completed – Order fulfilled and complete – requires no further action
    On-Hold – Awaiting payment – stock is reduced, but you need to confirm payment
    Cancelled – Cancelled by an admin or the customer – no further action required
    Refunded – Refunded by an admin – no further action required
*/

// su grandi wp_postmeta, l'aggiornamento può fallire senza avviso
// tramite questo array implemento un sistema di caching
$last_order_status = array();

// gli stati riguardano i dati trasmessi a F24, li uso per controllare le azioni crea PDF/ visualizza PDF / aggiorna PDF
function fatt_24_store_order_status($order_id, $status) {
	global $last_order_status;
	$rc = update_post_meta($order_id, FATT_24_ORDER_INVOICE_STATUS, $status);
	$last_order_status[$order_id] = $status;
}

// restituisce lo stato dell'ordine in F24
function fatt_24_get_order_status($order_id) {
	global $last_order_status;
	if (isset($last_order_status[$order_id]))
		return $last_order_status[$order_id];
	
	$retv = get_post_meta($order_id, FATT_24_ORDER_INVOICE_STATUS, true);
	return $retv;
}

// aggiunge i campi relativi allo stato dell'ordini e li restituisce aggiornati (la & nel parametro serve a questo)
function fatt_24_order_status_add_fields(&$status, $fields) {
	$h_save = array();
	if(!is_array($status))
		$status = array();
	
	foreach($fields as $k => $v) {
		if (isset($status[$k]))
			$h_save[$k] = $status[$k];
		$status[$k] = $v;
	}
	
	if (!empty($h_save)) {
		if (isset($status['history']))
			$h_save['history'] = $status['history'];
		$status['history'] = $h_save;
	}
}

// imposta i dati (sempre manipolando lo status)
function fatt_24_order_status_set_file_data(&$status, $pdfPath, $docType) {
	if ($status['docType'] == $docType)
		$status['pdfPath'] = $pdfPath;
	else if (isset($status['history']))
		fatt_24_order_status_set_file_data($status['history'], $pdfPath, $docType);
}

function fatt_24_order_status_set_doc_data(&$status, $returnCode, $description, $docId, $docType, $docNumber) { // added docNumber to wp_postmeta
		fatt_24_order_status_add_fields($status, compact('returnCode', 'description', 'docId', 'docType', 'docNumber')); // added docNumber
}

function fatt_24_order_status_set_error(&$status, $error) {
	fatt_24_order_status_add_fields($status, array('lastErr' => fatt_24_now().' : '.$error));
}

// restituisce lo stato dell'ordine
function fatt_24_order_status($order_id) {
	$rec = fatt_24_get_order_status($order_id);
	$status = FATT_24_INVSTA_NONE;
	$info = null;

	while ($rec) {
		if (isset($rec['pdfPath']) && fatt_24_peek($rec, 'docType') == FATT_24_DT_FATTURA || fatt_24_peek($rec, 'docType') == FATT_24_DT_FATTURA_ELETTRONICA) {
			$status = FATT_24_INVSTA_PDF_AVAIL_LOCAL;
			$info = $rec;
			break;
		}
		if (fatt_24_peek($rec, 'docId') && fatt_24_peek($rec, 'docType') == FATT_24_DT_FATTURA || fatt_24_peek($rec, 'docType') == FATT_24_DT_FATTURA_ELETTRONICA) {
			$status = FATT_24_INVSTA_PDF_AVAIL_SERVER;
			$info = $rec;
			break;
		}
		$rec = fatt_24_peek($rec, 'history');
	}
	
	$retv = array(
		'status'    =>$status,
		'info'      =>$info
	);
	return $retv;
}

// aggiungo i comandi ajax per gestire la creazione, visualizzazione e aggiornamento dei PDF
add_action('wp_ajax_invoice_admin_command', function() {
	$args = $_POST['args'];
    $order_id = $args['id'];
	/**
	 * con $doc_ajax_param passo alla chiamata ajax il valore scelto 
	 * dall'utente nelle impostazioni - sezione "Crea Fattura"
	 * Davide Iandoli 28.06.2019
	 */
    $doc_ajax_param = $args['type']; 
    check_ajax_referer(FATT_24_ORDER_ACTIONS_NONCE, 'security');
    $s = null;

    if ($args['cmd'] == 'upload') { // cmd == 'upload'
        if (fatt_24_store_fattura24_doc($order_id, $doc_ajax_param)) // passo il parametro della chiamata ajax
        {
            fatt_24_download_PDF($order_id);
            wp_send_json(array(1, fatt_24_actions_of_order($order_id, $doc_ajax_param, true))); // passo il parametro della chiamata ajax
        }
    } else {  // cmd == 'update'

        if ($s = fatt_24_get_order_status($order_id)) {
            if (fatt_24_download_PDF($order_id))
                wp_send_json(array(1, fatt_24_actions_of_order($order_id, $doc_ajax_param, true))); // passo il parametro della chiamata ajax
        }
    }
	
	if (!$s)
        $s = fatt_24_get_order_status($order_id);
    $err = 'function currently not available';

    if ($s) {
        if (isset($s['lastErr']))
            $err = $s['lastErr'];
        else if (isset($s['description']) && isset($s['returnCode']))
            $err = $s['description'];
    }
    wp_send_json(array(0, $err));
});

// creo e gestisco le azioni admin in base allo status dell'ordine
function fatt_24_admin_actions($status) {
	$r = array(
	     array('label' => __('Create PDF copy','fattura24'),  'cmd' => 'upload',   'enabled' => false), //Rimossa voce non funzionante - Davide 16.01.2019
	     array('label' => __('View PDF','fattura24'),        'cmd' => 'view',     'enabled' => false),
	     array('label' => __('Update PDF','fattura24'),      'cmd' => 'update', 'enabled' => false)
	);
   
	switch ($status) {
		case FATT_24_INVSTA_PDF_AVAIL_LOCAL:
			 $r[1]['enabled'] = // riabilita il download
			 $r[2]['enabled'] = true;
			 break;
		case FATT_24_INVSTA_PDF_AVAIL_SERVER:
			 $r[1]['enabled'] = true;
			 break;
		default:
			$r[0]['enabled'] = true;
			break;
	}
	return $r;
}

// restituisco l'indirizzo url del pdf dell'ordine
function fatt_24_order_PDF_url($order_id){
	$st = fatt_24_get_order_status($order_id);
	if ($st && isset($st['pdfPath']))
		return fatt_24_get_url_from_file($st['pdfPath']);
}

// con questo metodo 'pesco' da un array di risultati il valore che cerco
function fatt_24_peek($v, $k, $default = null) { 
	return isset($v[$k]) ? $v[$k] : $default; 
}

/**
 * Nuova funzione per aggiungere i pulsanti 'crea documento'
 * in modo selettivo nelle due colonne di controllo dello status
 * creata nuova funzione fatt_24_button in cui il terzo parametro 
 * definisce lo stile del pulsante. Se non viene definito il parametro è null
 */

function fatt_24_new_columns_action($order_id, $doc_ajax_param, $only_inner = false) 
{
	$s1 = fatt_24_order_status($order_id);
	$wp_nonce = wp_create_nonce(FATT_24_ORDER_ACTIONS_NONCE);
	$column_r = '';
	
	// qui creo il pulsante che verrà visualizzato solo a certe condizioni
	$cmd = function($cmd, $id) use ($doc_ajax_param, $wp_nonce)
    {
		$column_r = array('label' => __('Create document', 'fattura24'), 'cmd' => 'upload', 'enabled' => true);
		$cmd = $column_r['cmd'];
		$onclick = "f24_pdfcmd('$id','$cmd','$doc_ajax_param','$wp_nonce')"; // passo il parametro della chiamata ajax
		if ($column_r['enabled'])
			return fatt_24_button($onclick, $column_r['label'], 'button action'); // pulsante (con stile personalizzato) per creare il documento
		return $column_r['label'];	
	};
	
	$h = array($cmd($column_r, $order_id));	
    if ($only_inner) 
		$html = implode('<br>', $h);
	else
		$html = fatt_24_div(fatt_24_id('cmds-'.$order_id), implode('<br>', $h));
	
	return $html;		
}

/*
 * $doc_ajax_param è la nuova variabile che mi serve a distinguere il parametro passato al comando ajax
 * per non confonderlo con $docType e con $docParam
 * attraverso $docParam, con le modifiche fatte in api_call.php e behaviour.php, riesco a gestire anche le FE
 */

// aggiungo "Crea PDF, Visualizza PDF, Aggiorna PDF" al dettaglio dell'ordine 
function fatt_24_actions_of_order($order_id, $doc_ajax_param, $only_inner = false) // passo il parametro della chiamata ajax
{
   $s = fatt_24_order_status($order_id);
   $r = fatt_24_admin_actions($s['status']);
   $wp_nonce = wp_create_nonce(FATT_24_ORDER_ACTIONS_NONCE);
   $cmd = function($r, $id) use ($doc_ajax_param, $wp_nonce) 
   {
   		$cmd = $r['cmd'];
		$onclick = "f24_pdfcmd('$id','$cmd','$doc_ajax_param','$wp_nonce')"; // passo il parametro della chiamata ajax
		if ($r['enabled'])
			return fatt_24_a(array('href'=>'#', 'onclick'=>$onclick), $r['label']); // link per visualizzare il pdf
		return $r['label'];
	};
	
	$pdf = function($r, $order_id)
	{
		if ($r['enabled'])
			return fatt_24_a(array('href'=>fatt_24_order_PDF_url($order_id), 'target'=>'_blank'), $r['label']);
		return $r['label'];
	};
		
	$h = array(
		 $cmd($r[0], $order_id),
		 $pdf($r[1], $order_id),
		 $cmd($r[2], $order_id)
	);
	
	if ($only_inner) 
		$html = implode('<br>', $h);
	else
		$html = fatt_24_div(fatt_24_id('cmds-'.$order_id), implode('<br>', $h));
	return $html;
}

/*
* raggruppo le azioni in base alle opzioni scelte. Qui sotto processo la creazione della fattura
* necessario ad evitare duplicati quando metto l'ordine in stato completato più volte
*/

function fatt_24_process_fattura($order_id){
	/**
	* con $docParam passo alle funzioni fatt_24_is_available_on_f24
	* e fatt_24_store_fattura24_doc il tipo di documento
	* selezionato dall'utente nelle impostazioni
	* Davide Iandoli 28.06.2019
	*/
	$fatt_inv_create_new = get_option("fatt-24-inv-create");
	if ($fatt_inv_create_new == 1) {
		$docParam = FATT_24_DT_FATTURA;
	} else if ($fatt_inv_create_new == 2) {
		$docParam = FATT_24_DT_FATTURA_ELETTRONICA;
	}
	
	if(!is_null($docParam)) // se la creazione della fattura è disabilitata qui sotto non ci passo
		if (!fatt_24_is_available_on_f24($order_id, $docParam))
	    	fatt_24_store_fattura24_doc($order_id, $docParam);
}

// processo l'ordine
function fatt_24_process_order($order_id){
	if (fatt_24_get_flag(FATT_24_ORD_CREATE)) {
		if (!fatt_24_is_available_on_f24($order_id, FATT_24_DT_ORDINE))
			fatt_24_store_fattura24_doc($order_id, FATT_24_DT_ORDINE);
	//;
	}
}

// processo il cliente
function fatt_24_process_customer($order_id){
	if (fatt_24_get_flag(FATT_24_ABK_SAVE_CUST_DATA)){
		fatt_24_SaveCustomer($order_id);
	}
}

/*se il doc è creato in F24, ne restituisco il docId
 se il doc è una fattura ne restituisco anche il docNumber 
 nota: ora restituisco $resultArray invece di $docId
 codice modificato da Davide Iandoli 25.03.2020
*/
 function fatt_24_is_available_on_f24($order_id, $docType) {
	$s = fatt_24_get_order_status($order_id);
	
		while ($s){
			if (fatt_24_peek($s, 'docType') == $docType) {
				if ($docId = fatt_24_peek($s, 'docId'))
				{
					$resultArray['docId'] = $docId;
					if($docType != FATT_24_DT_ORDINE)
						if($docNumber = fatt_24_peek($s, 'docNumber'))	
							$resultArray['docNumber'] = $docNumber;
					return $resultArray;		
				}
			}
			$s = fatt_24_peek($s, 'history');
		}
		return null;
	
	
}

// se il PDF è disponibile ne restituisco il percorso e il $docType
function fatt_24_is_PDF_available($order_id, $docType){
	$s = fatt_24_get_order_status($order_id);
	
	while ($s)
	{
    	if (fatt_24_peek($s, 'docType') == $docType && fatt_24_peek($s, 'docId'))
		{
			if ($pdfPath = fatt_24_peek($s, 'pdfPath'))
				if (is_file($pdfPath))
					return $pdfPath;
		}
		$s = fatt_24_peek($s, 'history');
	}
	return null;
}