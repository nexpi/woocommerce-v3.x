<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * File di gestione delle impostazioni del plugin
 */
namespace fattura24;

if (!defined('ABSPATH'))
    exit;

require_once 'settings_uty.php';
require_once 'api_call.php';

if (is_admin())
{
    // versione del plugin f24
	function fatt_24_plugin_version(){
		if ($plugin_data = get_plugin_data(plugin_dir_path(__FILE__).'../fattura24.php'))
			return $plugin_data['Version'];
		throw new \Exception(__('no plugin information available', 'fattura24'));
	}

	// versione di woocommerce
	function fatt_24_woocommerce_version_check()
	{
		if(class_exists('WooCommerce'))
		{
			global $woocommerce;
			if(version_compare($woocommerce->version, '3.0.0', ">=" ))
				return true;
			else
				return false;
		}
		return true;
	}
		
	// controlla se c'è una nuova versione del plugin
	function fatt_24_checkNewVersion($urlCheckVersion)
	{
		try
			{
				$fileCheckVersion = fopen($urlCheckVersion, "r");
				if($fileCheckVersion)
				{
					$content = stream_get_contents($fileCheckVersion);
					fclose($fileCheckVersion);
					return $content;
				}
		}
		catch(Exception $e){
        	// non succede nulla
		}
		return false;
	}

	// restituisce il messaggio a valle del controllo sulla versione del plugin	
	function fatt_24_getVersionCheckMessage()
	{
		$pluginDate = '2020/03/31 16:00';
		$versionCheckMessage = '';
		$checkWooVersion = fatt_24_woocommerce_version_check(); // controllo la versione di woocommerce
		if(!$checkWooVersion)
		{
			$message = __('Warning: this plugin is not suitable for current WooCommerce version. Click ', 'fattura24');
			$message .= fatt_24_a(array('href' => 'https://www.fattura24.com/woocommerce-plugin-fatturazione', 'target' => '_blank'), __('here ', 'fattura24')); 
			$message .= __('to download the correct version of the plugin', 'fattura24');
			$versionCheckMessage .= fatt_24_getErrorHtml($message);
		}
		else
		{
			$urlCheckVersion = "https://www.fattura24.com/woocommerce/latest_version_woo_3.txt";
			$checkNewVersion = fatt_24_checkNewVersion($urlCheckVersion);
			if($checkNewVersion)
			{
				$latest_version_date = substr($checkNewVersion,0,16);
				if($latest_version_date != $pluginDate)
				{
					$message = __('A new version of the plugin has been released! Click ', 'fattura24'); // messaggio secondo lo standard WPML
					$message .= fatt_24_a(array('href' => substr($checkNewVersion,17), 'target' => '_blank'), __('here ', 'fattura24')); 
					$message .= __('to download', 'fattura24');
					$versionCheckMessage .= fatt_24_getNoticeHtml($message); // qui lo presento a video in una sola riga
				}
			}
		}
		return $versionCheckMessage;
	}
	
	// mostra il messaggio in una div apposita	
	function fatt_24_getNoticeHtml($message)
    {
        return '<div id="setting-error-settings_updated" class="notice notice-warning"><p><strong>' . $message . '</strong></p></div>';
    }

    // mostra il messaggio di errore in una div apposita
    function fatt_24_getErrorHtml($message)
    {
        return '<div id="setting-error-settings_updated" class="notice notice-error"><p><strong>' . $message . '</strong></p></div>';
    }
	
	// ottiene le informazioni sull'ambiente per includerle nel tracciato di log
    function fatt_24_getInfo()
    {
        global $woocommerce;
        global $wp_version;
        return ('Fattura24 plugin version: ' . fatt_24_plugin_version(). '\nWooCommerce version: ' . $woocommerce->version . '\nWordPress version: ' . $wp_version . '\nPHP version: ' . PHP_VERSION . '\nstore Url: ' . get_permalink(wc_get_page_id('shop')));
	}
}
    
// crea le sezioni della schermata di impostazioni principale e le gestisce
function fatt_24_init_settings()
{
	/* Intestazione */
	$sect_key = array(
	    'section_header'    =>__('Info','fattura24'), // new header
		'section_callback'  => null,
		'section_id'        =>FATT_24_OPT_SECT_ID,
		'fields'            =>array(
			FATT_24_OPT_PLUGIN_VERSION=>array(
				'type'  => 'label',
				'label' => __('Plugin version', 'fattura24'),
				'text'  => fatt_24_plugin_version(),
				'desc' => fatt_24_getVersionCheckMessage(),
			),
			FATT_24_OPT_API_KEY=>array(
				'type'  => 'text_cmd',
				'size'  => '40',
				'label' => __('Api Key', 'fattura24'),
				'cmd_id' => FATT_24_OPT_API_VERIFICATION, // ripristino pulsante in posizione originaria
				'cmd_text' => esc_html__('API Key Verify', 'fattura24'),
					   
			),
			FATT_24_API_MESSAGE=>array( // qui appare il messaggio che restituisce l'esito della verifica API
				'type' => 'label',
				'label' => '',
				'text' => fatt_24_strong(__('Warning: ', 'fattura24')) . __('Enter your API key and save settings before clicking on verify button', 'fattura24') // improved courtesy message
			)
		)
	);
	/* Sezione Rubrica */
	$sect_addrbook = array(
		'section_header'    => '<hr/>'.__('Address Book','fattura24'),
		'section_callback'  => null,
		'section_id'        => FATT_24_ABK_SECT_ID,
		'fields'            => array(
			FATT_24_ABK_SAVE_CUST_DATA=>array(
				'type'  => 'bool',
				'default' => true,
				'label' => __('Save Customer','fattura24'),
				'desc'  => __(' Enable saving Customer data on fattura24 address book','fattura24'),
				'help'  => __('Flag this box to save Customer data in Fattura24 address book','fattura24')
			),
			// permettere all’utente di impostare il C.F. e/o la P.IVA come campi obbligatori
			FATT_24_ABK_FISCODE_REQ=>array(
				'type'  => 'bool',
				'label' => __('Fiscal Code required','fattura24'),
				'desc'  => __(' Fiscal Code required', 'fattura24'),
				'help'  => __('Flag this box to make the Fiscal Code input field required if customer chose Italy as billing country', 'fattura24')
			),
			FATT_24_ABK_VATCODE_REQ=>array(
				'type'  => 'bool',
				'label' => __('Vat Code required', 'fattura24'),
				'desc'  => __(' Vat Code required', 'fattura24'),
				'help'  => __('Flag this box to make the Vat Code input field required', 'fattura24')
			)
		)
	);
	/* Sezione Ordini */
	$sect_orders = array(
		'section_header'    => '<hr/>'.__('Orders','fattura24'),
		'section_callback'  => null,
		'section_id'        => FATT_24_ORD_SECT_ID,
		'fields'            => array(
			FATT_24_ORD_CREATE=>array(
				'type'  => 'bool',
				'default' => true,
				'label' => __('Create order','fattura24'),
				'desc'  => __(' Enable order creation in Fattura24','fattura24'),
				'help'  => __('Flag this box to enable order creation in Fattura24','fattura24')
			),
			FATT_24_ORD_SEND=>array(
				'type'  => 'bool',
				'label' => __('Send email','fattura24'),
				'desc'  => __(' Email a PDF copy of Fattura24 order from F24 server','fattura24'),
				'help'  => __('Flag this box to email a PDF copy of Fattura24 order','fattura24')
			),
			FATT_24_ORD_STOCK=>array(
				'type'  => 'bool',
				'label' => __('Stock management','fattura24'),
				'desc'  => __(' Enable stock management for order creation','fattura24'),
				'help'  => __('Flag this box to enable stock management for F24 order creation. Products with the same code in Fattura24 and in WooCommerce will be reserved','fattura24')
			),
			FATT_24_ORD_TEMPLATE=>array(
				'type'  => 'select',
				'label' => __('Order template','fattura24'),
				'desc'  => __(' Select the template to create the PDF copy of an order','fattura24'),
				'help'  => __('Select the template you want to use to create a PDF copy of an order: this will be used in lack of a shipping address. Notice: to display the list save your Api Key in WooCommerce','fattura24')
			),
			FATT_24_ORD_TEMPLATE_DEST=>array(
				'type'  => 'select',
				'label' => __('Order template with destination','fattura24'),
				'desc'  => __(' Select the template to create PDF copy of an order which contains a shipping address','fattura24'),
				'help'  => __('Select the template you want to use to create PDF copy of an order: this will be used for orders containing a shipping address. Notice: to display the list save your Api Key in WooCommerce', 'fattura24')
			)
		)
	);
	/* Sezione Fattura */
	$sect_invoices = array(
		'section_header'    => '<hr/>'.__('Invoices','fattura24'),
		'section_callback'  => null,
		'section_id'        => FATT_24_INV_SECT_ID,
		'fields'            => array(
			FATT_24_INV_CREATE=>array(
				'type'  => 'select',
				'default' => '0',
				'options' => array(__('Disabled', 'fattura24'), __('Non-electronic Invoice','fattura24'), __('Electronic Invoice', 'fattura24')),
				'label' => __('Create invoice','fattura24'),
				'desc'  => __(' Enable Invoice creation and download automatically','fattura24'),
				'help'  => __('The Invoice about a customer order will be automatically created on Fattura24 server','fattura24')
			),
			FATT_24_ORD_STATUS_SELECT=>array( // aggiunta selezione degli stati dell'ordine
				'type'    => 'select',
				'default' => '0',
				'options' => array(__('Completed', 'fattura24'),__('Processing', 'fattura24')),
				'label'   => __('Order status', 'fattura24'),
				'help'    => __('Choose order status for automatic invoice creation', 'fattura24'), 
				'desc'    => __('Invoice will be created as soon as the order will be put in the selected status', 'fattura24')
			),
			FATT_24_INV_OBJECT=>array( // aggiunto campo causale del documento
				'type'    => 'text_cmd',
				'default' => 'Ordine E-Commerce (N)', // causale di default, altrimenti prende il dato immesso nel campo
				'label'   => __('Purpose of payment', 'fattura24'),
				'help'    => __('Put (N) in any place to add order number in document object', 'fattura24'),
				'desc'    => __('Put (N) in any place to add order number in document object', 'fattura24'),
				'cmd_id' => FATT_24_INV_DEFAULT_OBJECT, // aggiungo un tasto per l'oggetto predefinito
				'cmd_text' => esc_html__('Default', 'fattura24'),
			),
			FATT_24_INV_SEND=>array(
				'type'  => 'bool',
				'label' => __('Send email','fattura24'),
				'desc'  => __(' Send Invoice to customer by email when order status is completed','fattura24'),
				'help'  => __('Enable automatic sending of invoice from Fattura24 server to customer','fattura24')
			),
			FATT_24_INV_STOCK=>array(
				'type'  => 'bool',
				'label' => __('Stock management','fattura24'),
				'desc'  => __(' Enable stock management for invoice creation','fattura24'),
				'help'  => __('Flag this box to enable stock management for invoice creation. Products with the same code in Fattura24 and in WooCommerce will be unloaded','fattura24')
			),
			FATT_24_INV_WHEN_PAID=>array(
				'type'  => 'select', // cambiato in menu a discesa
				'label' => __('Status Paid','fattura24'),
				'default' => '0',
				'options' => array(__('Never', 'fattura24'),__('Always', 'fattura24'), __('E-payments (e.g.: Paypal)','fattura24')),
				'desc'  => __(' Create invoice in Paid status','fattura24'),
				'help'  => __('Choose a condition to create a document in Paid status','fattura24')
			),
			FATT_24_INV_DISABLE_RECEIPTS=>array(
				'type'  => 'bool',
				'label' => __('Disable creation of receipts','fattura24'),
				'desc'  => __(' Create an invoice instead of a receipt even in lack of customer VAT code','fattura24'),
				'help'  => __('Flag this box to enable the creation of an invoice instead of a receipt even in lack of customer VAT code','fattura24')
			),
			FATT_24_INV_TEMPLATE=>array(
				'type'  => 'select',
				'label' => __('Invoice template','fattura24'),
				'desc'  => __(' Select the template to create the PDF copy of an invoice','fattura24'),
				'help'  => __('Select the template you want to use to create the PDF copy of an invoice: this will be used in lack of a shipping address. Notice: to display the list save your Api Key in WooCommerce', 'fattura24')
			),
			FATT_24_INV_TEMPLATE_DEST=>array(
				'type'  => 'select',
				'label' => __('Invoice template with destination','fattura24'),
				'desc'  => __(' Select PDF template to create the PDF copy of an invoice','fattura24'),
				'help'  => __('Select the template you want to use to create the PDF copy of an invoice: this will be used for orders containing a shipping address. Notice: to display the list save your Api Key in WooCommerce', 'fattura24')
			),
			FATT_24_INV_PDC=>array(
				'type'  => 'select',
				'label' => __('Account planning','fattura24'),
				'desc'  => __(' Select the economic account that will be associated with the items of the documents','fattura24'),
				'help'  => __('Select the economic account that will be associated with the items of the invoice from your Fattura24 economic accounts. To display the list save your Api Key in WooCommerce','fattura24')
			),
			FATT_24_INV_SEZIONALE_RICEVUTA=>array(
				'type'  => 'select',
				'label' => __('Receipts issue number','fattura24'),
				'desc'  => __(' Select receipts issue number','fattura24'),
				'help'  => __('Select receipts issue number from the list of active issue numbers in Fattura24. To display this list save your Api Key in WordPress','fattura24')
			),
			FATT_24_INV_SEZIONALE_FATTURA=>array(
				'type'  => 'select',
				'label' => __('Invoices issue number','fattura24'),
				'desc'  => __(' Select invoices issue number','fattura24'),
				'help'  => __('Select invoices issue number from the list of active issue numbers in Fattura24. To display this list save your Api Key in WordPress','fattura24')
			)
		)
	);
	/* Sezione log */
	$sect_logs = array(
		'section_header'    => '<hr/>'.__('Log','fattura24'),
		'section_callback'  => null,
		'section_id'        => FATT_24_LOG_SECT_ID,
		'fields'            => array(
			FATT_24_LOG_DOWNLOAD=>array(
				'type'  => 'button',
				'label' => __('Download Log file', 'fattura24'),
				'text' => __('Download', 'fattura24'),
				'help'  => __('Warning: Fattura24 log file may be used for debugging, so WP debug mode must be active. Download and send Fattura24 log file to info@fattura24.com when you need help in troubleshooting', 'fattura24')
			),
			FATT_24_LOG_DELETE=>array(
				'type'  => 'button',
				'label' => __('Delete Log file', 'fattura24'),
				'text' => __('Delete', 'fattura24'),
				'help'  => __('Delete Fattura24 log file to clean space on disk or server.','fattura24')
			)
		)
	);
	fatt_24_setup_settings_page(FATT_24_SETTINGS_PAGE, FATT_24_SETTINGS_GROUP, array($sect_key, $sect_addrbook, /*$sect_docloc,*/ $sect_orders, $sect_invoices, $sect_logs));
}

// visualizza una specifica pagina di impostazioni
function fatt_24_show_settings() {
	fatt_24_get_link_and_logo(__('', 'fattura24'));
	 
	?>
   
	<div class='wrap'>
        
	 <nav class="nav-tab-wrapper woo-nav-tab-wrapper">
		<a href="?page=fatt-24-settings" class="nav-tab nav-tab-active"><?php _e('General Settings', 'fattura24')?></a>
		<a href="?page=fatt-24-tax" class="nav-tab "><?php _e('Tax Configuration', 'fattura24')?></a>
		<a href="?page=fatt-24-app" class="nav-tab "><?php _e('Mobile App', 'fattura24')?></a>	
		<a href="?page=fatt-24-warning" class="nav-tab "><?php echo fatt_24_notice(); _e(' Warning', 'fattura24')?></a>
	 </nav>
			
	 <form method='post' action='options.php'>
		<?php
			submit_button(__('Save Settings!', 'fattura24'), 'primary', 'submit_up'); // different id for submit buttons
			settings_fields(FATT_24_SETTINGS_GROUP);
			do_settings_sections(FATT_24_SETTINGS_PAGE);
			submit_button(__('Save Settings!', 'fattura24'), 'primary', 'submit_down');
		?>
	</form>

	</div>
	<script>
	function downloadFile(dataurl, filename)
	{
		var a = document.createElement("a");
		a.href = dataurl;
		a.setAttribute("download", filename);
		var b = document.createEvent("MouseEvents");
		b.initEvent("click", false, true);
		a.dispatchEvent(b);
			return false;
		}
		jQuery(function() {
			var $ = jQuery;
			<?php
				$api_key = get_option("fatt-24-API-key");
				//$save_cust = get_option("fatt-24-abk-save-cust-data");
				$ord_template = get_option("fatt-24-ord-template");
				$ord_template_dest = get_option("fatt-24-ord-template-dest");
				$inv_template = get_option("fatt-24-inv-template");
				$inv_template_dest = get_option("fatt-24-inv-template-dest");
				$inv_pdc = get_option("fatt-24-inv-pdc");
				$inv_sezionale_ricevuta = get_option("fatt-24-inv-sezionale-ricevuta");
				$inv_sezionale_fattura = get_option("fatt-24-inv-sezionale-fattura");
			?>
	            
			var ord_template_select = document.getElementById("fatt-24-ord-template");
			var ord_template_dest_select = document.getElementById("fatt-24-ord-template-dest");

			<?php
			foreach(fatt_24_getTemplate(true) as $modello_id => $modello_value)
			{ ?>
				var option = document.createElement("option");
				option.value = '<?= $modello_id ?>';
				option.text = '<?= $modello_value ?>';
				ord_template_select.add(option);
				ord_template_dest_select.add(option.cloneNode(true));
			<?php } ?>
			ord_template_select.value = '<?php
			if(empty($ord_template) || empty($api_key))
				echo 'Predefinito';
			else
				echo $ord_template;?>';
			ord_template_dest_select.value = '<?php
			if(empty($ord_template_dest) || empty($api_key))
				echo 'Predefinito';
			else
				echo $ord_template_dest;?>';
	
			var inv_template_create_select = document.getElementById("fatt-24-inv-create");
			var sel_inv_template_create = $('select[name="fatt-24-inv-create"]');
				
			<?php
				$sel_inv_create = get_option("fatt-24-inv-create");
			?>
			inv_template_create_select.value = '<?php echo $sel_inv_create ?>';
			setTimeout(function(){
				$('select[name="fatt-24-inv-create"]').trigger('change');
			},500);
				
			var inv_template_select = document.getElementById("fatt-24-inv-template");
			var inv_template_dest_select = document.getElementById("fatt-24-inv-template-dest");
			<?php
			foreach(fatt_24_getTemplate(false) as $modello_id => $modello_value)
			{ ?>
				var option = document.createElement("option");
				option.value = '<?= $modello_id ?>';
				option.text = '<?= $modello_value ?>';
				inv_template_select.add(option);
				inv_template_dest_select.add(option.cloneNode(true));
			<?php } ?>
			inv_template_select.value = '<?php
			if(empty($inv_template) || empty($api_key))
				echo 'Predefinito';
			else
				echo $inv_template;?>';
			inv_template_dest_select.value = '<?php
			if(empty($inv_template_dest) || empty($api_key))
				echo 'Predefinito';
			else
				echo $inv_template_dest;?>';
		
			var pdc_select = document.getElementById("fatt-24-inv-pdc");
			<?php
			foreach (fatt_24_getPdc() as $pdc_id => $pdc_value)
			{ ?>
				var option = document.createElement("option");
				option.value = '<?= $pdc_id ?>';
				option.text = '<?= $pdc_value ?>';
				pdc_select.add(option);
			<?php } ?>
			pdc_select.value = '<?php
			if(empty($inv_pdc) || empty($api_key))
				echo 'Nessun Pdc';
			else
				echo $inv_pdc;?>';
		
			var inv_sezionale_ricevuta_select = document.getElementById("fatt-24-inv-sezionale-ricevuta");
	
			<?php
			foreach (fatt_24_getSezionale(3) as $sezionale_id => $sezionale_value)
			{ ?>
				var option = document.createElement("option");
				option.value = '<?= $sezionale_id ?>';
				option.text = '<?= $sezionale_value ?>';
				inv_sezionale_ricevuta_select.add(option);
			<?php } ?>
			inv_sezionale_ricevuta_select.value = '<?php
			if(empty($inv_sezionale_ricevuta) || empty($api_key))
				echo 'Predefinito';
			else
				echo $inv_sezionale_ricevuta;?>';
			
			var inv_sezionale_fattura_select = document.getElementById("fatt-24-inv-sezionale-fattura");
			
			if(inv_template_create_select.value == 1){

			<?php
			foreach (fatt_24_getSezionale(1) as $sezionale_id => $sezionale_value)
			{ ?>
				var option = document.createElement("option");
				option.value = '<?= $sezionale_id ?>';
				option.text = '<?= $sezionale_value ?>';
				inv_sezionale_fattura_select.add(option);
			<?php } ?>
			inv_sezionale_fattura_select.value = '<?php
			if(empty($inv_sezionale_fattura) || empty($api_key))
				echo 'Predefinito';
			else
				echo $inv_sezionale_fattura;?>';
			}
			else if(inv_template_create_select.value == 2){

			<?php
			foreach (fatt_24_getSezionale(11) as $sezionale_id => $sezionale_value)
			{ ?>
				var option = document.createElement("option");
				option.value = '<?= $sezionale_id ?>';
				option.text = '<?= $sezionale_value ?>';
				inv_sezionale_fattura_select.add(option);

			<?php } ?>
			inv_sezionale_fattura_select.value = '<?php
			if(empty($inv_sezionale_fattura) || empty($api_key))
				echo 'Predefinito';
			else
				echo $inv_sezionale_fattura;?>';
			}

			var OPT_API_KEY = '#<?php echo FATT_24_OPT_API_KEY?>';
				
			/**
			* Modifica logica controllo API Key, non più chiamata ajax, l'xml viene letto direttamente da Javascript
			* Parametri: apiKey= (api-key)
			*/
			var btn = document.getElementById("fatt-24-API-verification"); // id del pulsante di verifica
				btn.addEventListener('click', function (e) {
					var xhr = new XMLHttpRequest();
					var apiKey = "<?php echo get_option("fatt-24-API-key"); ?>"; // ottiene l'API Key
					var params = "apiKey=" + apiKey; // la passa

					xhr.open("post","https://www.app.fattura24.com/api/v0.3/TestKey", true);
					xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
					xhr.send(params);
					xhr.onload = function () {
                		console.log(this.responseText);
                		var response = this.responseText.replace("&egrave;", ""); //devo togliere questa parte altrimenti da problemi con l'encoding xml
                	
						console.log(response);
                		var parser = new DOMParser(); // legge il risultato xml
                		var xml = parser.parseFromString(response, "text/xml");
                		var code = xml.getElementsByTagName('returnCode');
  
                		var description = xml.getElementsByTagName('description');
                		var type = xml.getElementsByTagName('type'); // tipo di abbonamento
						var expire = xml.getElementsByTagName('expire'); // data di scadenza
                  
                		if (code[0].childNodes[0].nodeValue == 1) {
							/**
							 * Qui gestisco gli errori se l'albero xml non è completo
							 * in particolare se manca la data di scadenza. Il testo risultante sostituisce quello predefinito
							 * grazie alla proprietà innerHtml
							 */
							try {  
						    	document.getElementById("fatt-24-api-message").innerHTML = "<span style='color: green; font-size: 120%;'>chiave API verificata! </span><br/>Scadenza abbonamento: "+ expire[0].childNodes[0].nodeValue ; 
							}
							catch (err) {
								document.getElementById("fatt-24-api-message").innerHTML = "<span style='color: red; font-size: 120%;'>Account di Test! </span></br>"; // solo gli account di test sono senza scadenza
							}
                     	
						} else if (!apiKey) {
							document.getElementById("fatt-24-api-message").innerHTML = "<span style='color: red; font-size: 120%;'>chiave API non inserita! </span></br>"; 
					
						} else {
							/**
							* Qui cerco di ottenere il tipo di abbonamento per includere un messaggio di errore specifico
							* se non lo trovo visualizzo il messaggio generico (Davide Iandoli 24.01.2020)
							*/
							try {
								if (type[0].childNodes[0].nodeValue < 5) { 
									document.getElementById("fatt-24-api-message").innerHTML = "<span style='color: red; font-size: 120%;'>Per usare il servizio è necessario un abbonamento Business o Enterprise! </span></br>";
								}
							}
							catch (err) {
							 	document.getElementById("fatt-24-api-message").innerHTML = "<span style='color: red; font-size: 120%;'>API Key non valida! </span></br>";
							}	
						}
					}
        		});
            /**
			* Pulsante per inserire l'oggetto predefinito di F24 nel campo causale
			*/
			var dfbtn = document.getElementById('fatt-24-inv-default-object'); // dfbtn => default_button

			dfbtn.addEventListener('click', function () {
				document.getElementById('fatt-24-inv-object').defaultValue = 'Ordine E-commerce (N)';
			});
				
			var plugin_path = '<?php echo plugin_dir_url(__FILE__)?>';
			var hwe_nonce = '<?php echo wp_create_nonce('hwe_verify_custom_nonce'); ?>';
			var LOG_DOWNLOAD = '#<?php echo FATT_24_LOG_DOWNLOAD?>';
			var WP_DEBUG = '<?= WP_DEBUG ?>'; // voglio sapere se il debug di WP è attivo
							
			$(LOG_DOWNLOAD).click(function()
			{
				$.ajax({
					method: 'POST',
					data: 'info='+'<?php echo fatt_24_getInfo() ?>&nonce='+hwe_nonce+'&action=hwedonwloadlogs',
					url: '<?php echo admin_url( 'admin-ajax.php' );?>',
					dataType: 'json',
											
					success: function(response)
					{   
						if(!WP_DEBUG){
							alert('Il file di log è vuoto. Per scriverlo e scaricarlo devi attivare la modalità di debug di Wordpress');
						}else if(response.t == 1 || response.t == 2){  // prova a scaricare il log anche se l'accesso avviene dall'esterno
							// da qui rinomino il file solo per il click su 'download'
							var today = new Date();
							var dateformat = today.getDate() + '-' + (today.getMonth()+1) + '-' + today.getFullYear() + '_' + today.getHours() + '_' + today.getMinutes();
							var new_filename = 'f24_trace_' + dateformat + '.log';
							downloadFile(plugin_path + 'trace.log', new_filename); // rinomino il file solo al download
						}
						else{
							alert('Errore durante il download.');	
						}
					}
				});
			});
				
			var hwe_nonce2 = '<?php echo wp_create_nonce('hwe_delete_logs_nonce'); ?>';
			var LOG_DELETE = '#<?php echo FATT_24_LOG_DELETE?>';
			$(LOG_DELETE).click(function()
			{
				var r = confirm("Sei sicuro di voler cancellare il file di log di Fattura24?");
				if (r == true)
				{
					$.ajax({
						url: '<?php echo admin_url( 'admin-ajax.php' );?>',
						data: {action:'hwe_deletelogs', nonce : hwe_nonce2 },
						dataType: 'json',
						method: 'POST',
						success: function (response)
						{
							if(response.fileExists === true)
							{
								alert('File cancellato! La sua dimensione era: ' + response.size + ' KB.');
							}
							else if(response.fileExists === false)
							{
								alert('Il file è già stato cancellato.');
							}
						}
					});
				}
			});
				
			// abilita e disabilita opzioni per le caselle di controllo
			function cb_controlled(main, controlled) {
				var checked = $(main).is(':checked')
				var group = controlled.join(',')
				if (checked) {
					$(group).prop('disabled', false);
				}
				else {
					$(group).prop('disabled', true)
					$(group).prop('checked', false)
				}
			}
			
			// abilita e disabilita opzioni per i menu a discesa
			function select_controlled(main, controlled) {
				var selectedVal = $(main).val();
				var group = controlled.join(',');
				$('table tr.electronic_invoice_row').hide();
	
				if (selectedVal != '' && selectedVal == 1) {
					$(group).prop('disabled', false);
				}else if(selectedVal == '2') {
					strArray = group.split(',');
					for (var i = 0; i < strArray.length; i++) {
						if (strArray[i] === '#<?php echo FATT_24_INV_SEND ?>') {
							strArray.splice(i, 1);
						}
					}
					group = strArray.join(',');
					$(group).prop('disabled', false);
					$('table tr.electronic_invoice_row').show();
				}
				else {
					$(group).prop('disabled', true)
					$(group).prop('checked', false)
				}
			}
	
			var SAVE_CUST = document.getElementById("fatt-24-abk-save-cust-data");
			var	ORD_CREATE            = '#<?php echo FATT_24_ORD_CREATE?>',
				ORD_SEND              = '#<?php echo FATT_24_ORD_SEND?>',
				ORD_STOCK             = '#<?php echo FATT_24_ORD_STOCK?>',
				ORD_TEMPLATE          = '#<?php echo FATT_24_ORD_TEMPLATE?>',
				ORD_TEMPLATE_DEST     = '#<?php echo FATT_24_ORD_TEMPLATE_DEST?>';
			var INV_CREATE            = '#<?php echo FATT_24_INV_CREATE?>',
				INV_SEND              = '#<?php echo FATT_24_INV_SEND?>',
				INV_STOCK             = '#<?php echo FATT_24_INV_STOCK?>',
				INV_WHEN_PAID        = '#<?php echo FATT_24_INV_WHEN_PAID?>',
				INV_DISABLE_RECEIPTS  = '#<?php echo FATT_24_INV_DISABLE_RECEIPTS?>',
				INV_TEMPLATE          = '#<?php echo FATT_24_INV_TEMPLATE?>',
				INV_TEMPLATE_DEST     = '#<?php echo FATT_24_INV_TEMPLATE_DEST?>',
				INV_PDC               = '#<?php echo FATT_24_INV_PDC?>',
				INV_SEZIONALE_RICEVUTA= '#<?php echo FATT_24_INV_SEZIONALE_RICEVUTA?>',
				INV_SEZIONALE_FATTURA = '#<?php echo FATT_24_INV_SEZIONALE_FATTURA?>';
				
				$(ORD_CREATE).click(function() {
					cb_controlled(ORD_CREATE, [ORD_SEND, ORD_STOCK, ORD_TEMPLATE, ORD_TEMPLATE_DEST])
				})
				$(INV_CREATE).change(function() {
					select_controlled(INV_CREATE, [INV_SEND, INV_STOCK, INV_WHEN_PAID, INV_DISABLE_RECEIPTS,
					INV_TEMPLATE, INV_TEMPLATE_DEST, INV_PDC, INV_SEZIONALE_RICEVUTA, INV_SEZIONALE_FATTURA])
				})
	
				cb_controlled(ORD_CREATE, [ORD_SEND, ORD_STOCK, ORD_TEMPLATE, ORD_TEMPLATE_DEST])
				select_controlled(INV_CREATE, [INV_SEND, INV_STOCK, INV_WHEN_PAID, INV_DISABLE_RECEIPTS, 
					INV_TEMPLATE, INV_TEMPLATE_DEST, INV_PDC, INV_SEZIONALE_RICEVUTA, INV_SEZIONALE_FATTURA])
				
			// funzione che abilita la checkbox "salva cliente" solo se crea ordine == vuota e crea fattura == disabilitata
			function enable_when_both_off() {
				 var c1 = (($(INV_CREATE).val() != 0 ) ? true : false);
				 var c2 = $(ORD_CREATE).is(':checked');
				 var c3 = $(SAVE_CUST).is(':checked'); // controllo il valore è salvo
				   
				 if(c1 || c2) {
				 	$(SAVE_CUST).prop('checked', true);
					$(SAVE_CUST).prop('disabled', true);
				 } else  {
					$(SAVE_CUST).prop('disabled', false); 
					$(SAVE_CUST).prop('checked', !c3); // predefinito = true, così lo cambio in false 
				}	
			}
            /**
			* Abilito la casella invia Email solo se ho scelto la fattura tradizionale
			*/
			function disable_send_invoice(){
				var option = (($(INV_CREATE).val() == 1) ? false : true)
				$(INV_SEND).prop('disabled', option)
			}
	
			$(ORD_CREATE).click(function() {
				enable_when_both_off();
			})
	
			$(INV_CREATE).change(function(){
				enable_when_both_off();
				disable_send_invoice();
			});
	
			enable_when_both_off();
		})
	</script>
<?php
}
