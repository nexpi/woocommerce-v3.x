<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * helpers per costruire le schermata di impostazioni del plugin
 * 
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'uty.php';
require_once 'constants.php';

// gestisco la pagina delle impostazioni del plugin
function fatt_24_setup_settings_page($page_id, $group_id, $sections) {
	//sezioni	
	foreach($sections as $sect) {
		add_settings_section(
			$sect['section_id'],
			$sect['section_header'],
			$sect['section_callback'],
			$page_id
		);
		// campi della sezione
		foreach($sect['fields'] as $id => $def) {
			$args = array('id' => $id);
			if (isset($def['help']))
				$args['help'] = fatt_24_array_string($def['help'], "\n");
		
			foreach(array('type', 'desc', 'text', 'size', 'default', 'cmd_id', 'cmd_text', 'cmd_help', 'func', 'class', 'options') as $arg)
				if (isset($def[$arg]))
					$args[$arg] = $def[$arg];
  	
    		add_settings_field(
				$id,
				$def['label'],
				__NAMESPACE__ .'\setting_field_'.$def['type'].'_callback',
				$page_id,
				$sect['section_id'],
				$args
			);
			register_setting($group_id, $id);
		}
	}
}

// visualizza un'icona di help scelta tra le dashicon di WP
function fatt_24_helpico($text) {
	return fatt_24_span(array('title'=>$text, 'class'=>'dashicons dashicons-editor-help', 'style'=>'margin-left:5px'), array());
}

// visualizza l'icona di un megafono dalle dashicon di WP
function fatt_24_notice() {
	return fatt_24_span(array('class'=>'dashicons dashicons-megaphone'), array());
}


function fatt_24_setting_field_output($widget, $help, $desc) {
	echo apply_filters(FATT_24_LAYOUT_OPTION, compact('widget', 'help', 'desc'));
	/*
	echo $widget.'<br>';
	if ($help) echo helpico($help);
	if ($desc) echo $desc;
	*/
}




// widget checkbox
function fatt_24_widget_bool($id, $class) {
	return fatt_24_input(array(
		'name'  => $id,
	   	'id'    => $id,
		'type'  => 'checkbox',
		'value' => '1',
		'class' => $class,
		checked(1, get_option($id), false)
	));
}
// chiamata associata alla checkbox
function setting_field_bool_callback(array $args) {
	extract(shortcode_atts(array(
		'id'    => null,
		'desc'  => null,
		'help'  => null,
		'default' => null
	),  $args));

	if ($default !== null) {
		global $wpdb;
		$c = $wpdb->get_results("select * from $wpdb->options where option_name='$id'");
		if ($c === array())
			update_option($id, $default);
	}
	
	$widget = fatt_24_widget_bool($id, 'code');
	fatt_24_setting_field_output($widget, $help, $desc);
}

// widget radio
function fatt_24_widget_radio($id, $class, $options) {
	$widget = '';
	foreach($options as $k => $v) {
		$ref = sprintf('%s-%s', $id, $k);
		$widget .=
		fatt_24_label(array('for'=>$ref), $v) .
		fatt_24_input(array('name'=>$id, 'id'=>$ref, 'type'=>'radio', 'value'=>$k, 'class'=>$class, 0=>checked($k, get_option($id), false)));
  	}
	return $widget;
}

// chiamata associata al widget radio
function setting_field_radio_callback(array $args) {
	extract(shortcode_atts(array(
		'id'        => null,
		'desc'      => null,
		'help'      => null,
		'options'   => array(),
		'class'     => null
	),  $args));
	$widget = fatt_24_widget_radio($id, $class, $options);
	fatt_24_setting_field_output($widget, $help, $desc);
}

// widget tendina: restituisce le opzioni del menu
function fatt_24_widget_select($id, $class, $current, $options) {
	return fatt_24_select(array('name'=>$id, 'id'=>$id, 'class'=>$class),

	fatt_24_array_map_kv(function($k, $v) use($current) {
			return fatt_24_option(array('value' => $k, selected($k, $current, false)), $v);
	}, $options));
}

// chiamata associata al menu a tendina
function setting_field_select_callback(array $args) {
	extract(shortcode_atts(array(
		'id'        => null,
		'class'     => null,
		'options'   => array(),
		'desc'      => null,
		'help'      => null,
	),  $args));
	
	$current = get_option($id);
	$widget = fatt_24_widget_select($id, $class, $current, $options);
	fatt_24_setting_field_output($widget, $help, $desc);
}

// campo di input testo
function setting_field_text_callback(array $args) {
	extract(shortcode_atts(array(
		'id'    => null,
		'desc'  => null,
		'help'  => null,
		'class' => null,
		'size'  => 32,
		'default' => null
	),  $args));
	
	$value = get_option($id);
	if (empty($value))
		$value = $default;
		
	fatt_24_setting_field_output(
		fatt_24_input(array(
			'type'  => 'text',
			'name'  => $id,
			'id'    => $id,
			'value' => $value,
			'class' => $class,
			'size'  => $size,
		)), $help, $desc);
}

// gestione pulsante
function setting_field_button_callback(array $args) {
	extract(shortcode_atts(array(
    	'id'    => null,
		'desc'  => null,
		'help'  => null,
		'text'  => null
	),  $args));

	fatt_24_setting_field_output(
		fatt_24_input(array(
			'type'  => 'button',
			'name'  => $id,
			'id'    => $id,
			'class' => 'code',
			'value' => $text
		)), $help, $desc);
}

// chiamata associata all'etichetta
function setting_field_label_callback(array $args) {
	extract(shortcode_atts(array(
		'id'    => null,
		'desc'  => null,
		'help'  => null,
		'text'  => null,
	),  $args));
	fatt_24_setting_field_output(fatt_24_label(fatt_24_id($id), $text), $help, $desc);
}

// chiamata associata al pulsante
function setting_field_text_cmd_callback(array $args) {
	extract(shortcode_atts(array(
		'id'        => null,
		'desc'      => null,
		'help'      => null,
		'size'      => 32,
		'class'     => 'code',
		'default'   => null,
		'cmd_id'    => null,
		'cmd_text'  => null,
		'cmd_help'  => null
	),  $args));
	
	$widget = fatt_24_span(array(
		fatt_24_input(array(
			'type'  => 'text',
			'name'  => $id,
			'id'    => $id,
			'value' => get_option($id, $default),
			'class' => $class,
			'size'  => $size
		)),
		fatt_24_input(array(
			'type'  => 'button',
			'id'    => $cmd_id,
			'class' => 'code',
			'value' => $cmd_text,
	   		'title' => $cmd_help
    	))
	));
	fatt_24_setting_field_output($widget, $help, $desc);
}

// gestione tabelle
function setting_field_table_callback(array $args) {
	extract(shortcode_atts(array(
		'id'    => null,
		'desc'  => null,
		'help'  => null,
		'func'  => null,
	),  $args));
	assert('$id != null');
	fatt_24_setting_field_output(fatt_24_label(id($id), call_user_func($func)), $help, $desc);
}
