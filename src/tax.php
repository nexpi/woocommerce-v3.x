<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: gestisce la tab "Configurazione Tassa" della schermata di impostazioni
 */
namespace fattura24;

if (!defined('ABSPATH'))
   exit;

/*if (is_admin())
{
	//@session_start();
}*/

// visualizza la schermata "Configurazione Tassa"
function fatt_24_show_tax() {
    global $wpdb;
	$table_name = $wpdb->prefix . "fattura_tax";
	$tax_id = ''; // definisco $tax_id
	$msg='';

    if (isset($_POST['insert'])) {
		
	   $tax_id = sanitize_text_field($_POST["tax_id"]);
	   $tax_code = sanitize_text_field($_POST["tax_code"]); 
	   $id = sanitize_text_field($_GET['id']);

	   if(empty($tax_id)){
		   $msg=__('Please select Tax Name','fattura24');

	   } else if ($tax_code == "Scegli") {
		   $msg=__('Please enter Natura','fattura24');

	   } else if (isset($_GET['id'])){
		   $row = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name  where tax_id=%s AND id!=%s", $tax_id,$id));

			if(count($row)==0){
    			$wpdb->update(
                	  $table_name, //table
                	  array('tax_id' => $tax_id, 'tax_code' => $tax_code), //data
                      array('id' => $id), //where
                      array('%s'), //data format
                      array('%s','%s') //where format
				);

				$_SESSION['message']=__('Record has been updated successfully','fattura24');
				wp_redirect('admin.php?page=fatt-24-tax');
				exit;

			} else {
				$msg=__('Tax code already assigned with the different Tax. please change the Tax','fattura24');
			}

	   } else {
		    $row = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name  where tax_id=%s", $tax_id));

			if(count($row)==0){
				$wpdb->insert(
	                   $table_name, //table
					   array('tax_id' => $tax_id, 'tax_code' => $tax_code), //data
					   array('%d', '%s') //data format			
				);

				$_SESSION['message']=__('Record added successfully','fattura24');
   				wp_redirect('admin.php?page=fatt-24-tax');
				exit;

			} else {
				$msg=__('Duplicate entry found with the same Tax','fattura24');
			}
	   }

	} else if (isset($_GET['del'])) {

		$id=sanitize_text_field($_GET['del']);
        $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE id = %s", $id));
		$_SESSION['message']=__('Record has been deleted successfully','fattura24');
		wp_redirect('admin.php?page=fatt-24-tax');
		exit;

    } else if (isset($_GET['id'])) {
        $row = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name  where id=%s", $id));

	    if($row>0){
		   $row=$row[0];
		   $id=$row->id;
		   $tax_code=$row->tax_code;
           $tax_id=$row->tax_id;
    	}
    }
    ?>

	<?php fatt_24_get_link_and_logo(__('', 'fatt-24-tax')); ?>

	<div class='wrap'>
    
       <nav class="nav-tab-wrapper woo-nav-tab-wrapper">
            <a href="?page=fatt-24-settings"  class="nav-tab "><?php _e('General Settings', 'fattura24')?></a>	
            <a href="?page=fatt-24-tax" class="nav-tab nav-tab-active"><?php _e('Tax Configuration', 'fattura24')?></a>
			<a href="?page=fatt-24-app" class="nav-tab "><?php _e('Mobile App', 'fattura24')?></a>
			<a href="?page=fatt-24-warning" class="nav-tab "><?php echo fatt_24_notice(); _e(' Warning', 'fattura24')?></a>
       </nav>

    <?php if($msg){?>
    	<div class="notice notice-warning is-dismissible">
         	<p><?php echo $msg ?></p>
    	</div>
    <?php } ?>
    
	<?php if(isset($_SESSION['message'])){?>
    	<div class="notice notice-success is-dismissible">
        	<p><?php echo $_SESSION['message']; ?></p>
    	</div>
    <?php unset($_SESSION['message']); } ?>

        <form method='post' >
    		<table class='wp-list-table widefat fixed'>
                <tr>
                   <th class="ss-th-width" width="15%"><?php echo __('Tax','fattura24'); ?></th>
                    	<td>
						<?php 
                          $zero_rate = 0.0; // con questa variabile seleziono solo le aliquote a tasso zero
		                  $sql="SELECT * from ".$wpdb->prefix."woocommerce_tax_rates where tax_rate = " . $zero_rate .  " order by tax_rate_id desc"; // la query ora controlla la percentuale e non la classe
                    	  $tax_rate = $wpdb->get_results($sql);
                          $select = "<select name='tax_id' class='postform'>";
                          $select.= "<option value=''>".__('Select Tax','fattura24')."</option>";

                          foreach($tax_rate as $category){
							 if ($category->tax_rate_id==$tax_id)
		                        $select.= "<option value='".$category->tax_rate_id."' selected='selected'>".$category->tax_rate_name."</option>";
                           	 else
                       			$select.= "<option value='".$category->tax_rate_id."'>".$category->tax_rate_name."</option>";
                          }
						  
						  $select.= "</select>";
						  echo $select; 
						  
						  /**
						   * Qui creo le opzioni per la corretta gestione della Natura IVA
						   * quelle commentate corrispondono alle nuove specifiche tecniche dell'AdE
						   * in vigore dal 5 maggio 2020. Da notare l'uso del tag <optgroup> per raggruppamento 
						   * e migliore gestione della selezione - Davide Iandoli 17.03.2020
						   */

	                      $html = __("<select name='tax_code' class='postform'>", 'fattura24');
						  $html .= __("<option value='Scegli' > Choose an item... </option>", 'fattura24');
						  //$html .= __("<optgroup label='N1'>", 'fattura24');
						  $html .= __("<option value='N1'> N1 - escluse ex art.15 </option>", 'fattura24');
						  //$html .= __("</optgroup>", 'fattura24');
						  //$html .= __("<optgroup label='N2'>", 'fattura24');
						  $html .= __("<option value='N2'> N2 - non soggette </option>", 'fattura24');
						  //$html .= __("<option value='N2.1'> N2.1 - non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72 </option>", 'fattura24');
						  //$html .= __("<option value='N2.2'> N2.2 - non soggette - altri casi </option>", 'fattura24');
						  //$html .= __("</optgroup>", 'fattura24');
						  //$html .= __("<optgroup label='N3'>", 'fattura24');
						  $html .= __("<option value='N3'> N3 - non imponibili </option>", 'fattura24');
						  /*$html .= __("<option value='N3.1'> N3.1 - non imponibili - esportazioni </option>", 'fattura24');
						  $html .= __("<option value='N3.2'> N3.2 - non imponibili  - cessioni intracomunitarie </option>", 'fattura24');
        			      $html .= __("<option value='N3.3'> N3.3 - non imponibili  - cessioni verso San Marino </option>", 'fattura24');
						  $html .= __("<option value='N3.4'> N3.4 - non imponibili  - operazioni assimilate alle cessioni all'esportazione </option>", 'fattura24');
        			      $html .= __("<option value='N3.5'> N3.5 - non imponibili  - a seguito di dichiarazioni d'intento </option>", 'fattura24');
        			      $html .= __("<option value='N3.6'> N3.6 - non imponibili - altre operazioni che non concorrono alla formazione del plafond </option>", 'fattura24');
						  $html .= __("</optgroup>", 'fattura24');
						  $html .= __("<optgroup label='N4'>", 'fattura24');*/
						  $html .= __("<option value='N4'> N4 - esenti </option>", 'fattura24');
						  //$html .= __("</optgroup>", 'fattura24');
						  //$html .= __("<optgroup label='N5'>", 'fattura24');
						  $html .= __("<option value='N5'> N5 - regime del margine / Iva non esposta in fattura </option>", 'fattura24');
						  //$html .= __("</optgroup>", 'fattura24');
						  //$html .= __("<optgroup label='N6'>", 'fattura24');
						  $html .= __("<option value='N6'> N6 - inversione contabile </option>", 'fattura24');
						  /*$html .= __("<option value='N6.1'> N6.1 - inversione contabile - cessione di rottami e altri materiali di recupero </option>", 'fattura24');
						  $html .= __("<option value='N6.2'> N6.2 - inversione contabile - cessione di oro e argento puro </option>", 'fattura24');
						  $html .= __("<option value='N6.3'> N6.3 - inversione contabile - subappalto nel settore edile </option>", 'fattura24');
						  $html .= __("<option value='N6.4'> N6.4 - inversione contabile - cessione di fabbricati - cessione di fabbricati </option>", 'fattura24');
						  $html .= __("<option value='N6.5'> N6.5 - inversione contabile - cessione di telefoni cellulari </option>", 'fattura24');
						  $html .= __("<option value='N6.6'> N6.6 - inversione contabile - cessione di prodotti elettronici </option>", 'fattura24');
						  $html .= __("<option value='N6.7'> N6.7 - inversione contabile - prestazioni comparto edile e settori connessi </option>", 'fattura24');
						  $html .= __("<option value='N6.8'> N6.8 - inversione contabile - operazioni settore energetico </option>", 'fattura24');
						  $html .= __("<option value='N6.9'> N6.9 - inversione contabile - altri casi </option>", 'fattura24');
						  $html .= __("</optgroup>", 'fattura24');
						  $html .= __("<optgroup label='N7'>", 'fattura24');*/
						  $html .= __("<option value='N7'> N7 - IVA assolta in altro stato UE </option>", 'fattura24');
						  //$html .= __("</optgroup>", 'fattura24');
        				  $html .= __("</select>", 'fattura24');

 						?>
						</td>
                </tr>
                <tr>
                    <th class="ss-th-width"><?php _e('Natura','fattura24'); ?></th>
     				<td><?php echo $html; ?></td>
                </tr>
				<tr>
                    <th class="ss-th-width"></th>
                    <td> <input type='submit' name="insert" value=<?php _e('Save', 'fattura24')?> class='button'></td>
                </tr>
            </table>
		</form>
	</div>

    <div class='wrap'>

        <?php
            $sql="SELECT m.id,m.tax_id,m.tax_code,t.tax_rate_name from $table_name as m LEFT JOIN ".$wpdb->prefix."woocommerce_tax_rates as t ON (m.tax_id=t.tax_rate_id) order by m.id desc";
            $rows = $wpdb->get_results($sql);
        ?>

        <table class='wp-list-table widefat fixed striped pages'>
			<thead>
            <tr>
                <th class="manage-column ss-list-width" width="80"><?php echo __('ID','fattura24'); ?></th>
                <th class="manage-column ss-list-width"><?php _e('Name','fattura24'); ?></th>
                <th class="manage-column ss-list-width"><?php _e('Natura','fattura24'); ?></th>
                <th colspan="2" width="150"><?php _e('Action','fattura24'); ?></th>
            </tr>
            </thead>
			<tbody>
				<?php foreach ($rows as $row) { ?>
           			<tr>
               			<td class="manage-column ss-list-width"><?php echo $row->id; ?></td>
               			<td class="manage-column ss-list-width"><?php echo $row->tax_rate_name; ?></td>
               			<td class="manage-column ss-list-width"><?php echo $row->tax_code; ?></td>
               			<td><a href="<?php echo admin_url('admin.php?page=fatt-24-tax&id=' . $row->id); ?>"><?php echo __('Edit','fattura24'); ?></a></td>
		    			<td><a href="<?php echo admin_url('admin.php?page=fatt-24-tax&del=' . $row->id); ?>"><?php echo __('Delete','fattura24'); ?></a></td>
           			</tr>
       			<?php } ?>
   			</tbody>
        </table>
    </div>
	<div class="wrap">
    	<p style="font-size:150%; font-weight:bold;"><?php _e('User instructions', 'fattura24') ?></p>
	    <ol>
		  	<li style ="font-size:120%;"><?php _e('Configure in Woocommerce->Settings->Tax one ore more zero rated taxes', 'fattura24')?></li>
			<li style ="font-size:120%;"><?php _e('Warning: tax name should match the legal reference / natura.', 'fattura24')?></li>
			<li style ="font-size:120%;"><?php _e('Save the changes: first dropdown will list all rates configured.', 'fattura24')?></li></td></tr>
			<li style ="font-size:120%;"><?php _e('Choose natura and save the changes.', 'fattura24')?></li>
    	</ol>
		<p style = "padding:10px; font-size:120%;"><?php _e('Here below an explanatory picture.', 'fattura24')?></p>
		<p><?php echo fatt_24_img(fatt_24_attr('src', fatt_24_png('../assets/fattura24tax')), array())?></p>
	</div>
    <?php
}
