<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: gestisce componenti di debug, helpers per array e helpers html
 * 
 */
namespace fattura24;

if (!defined('ABSPATH')) exit;

// con questo metodo ottengo l'orario attuale utilizzando il fuso orario di Roma
function fatt_24_now($fmt = 'Y-m-d H:i:s', $tz = 'Europe/Rome') {
    $timestamp = time();
    $dt = new \DateTime("now", new \DateTimeZone($tz)); //il primo param dev'essere una stringa
	$dt->setTimestamp($timestamp); //ottiene il timestamp attuale
	return $dt->format($fmt);
}

// scrive nel file di tracciato
function fatt_24_trace() {
    if ($f = @fopen(plugin_dir_path(__FILE__).'trace.log', 'a')) {
		fprintf($f, "%s: %s\n", fatt_24_now(), var_export(func_get_args(), true));
		fclose($f);
	}
}

// tracciato specifico per le funzioni
function fatt_24_ftrace() {
	if (true) {
        $args = func_get_args();
		$file = $args[0];
		$path_parts = pathinfo($file);
		$exclude = array();

        if (array_search($path_parts['filename'], $exclude) === false)
			if ($f = @fopen(plugin_dir_path($file).'trace.log', 'a')) {
				fprintf($f, "%s: %s\n", fatt_24_now(), var_export(array_slice($args, 1), true));
				fclose($f);
			}
	}
}

// gestisco errori fatali
function fatt_24_fatal($why) {
    fatt_24_trace('fatal', $why, debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
	throw new \Exception('fatal error:'.$why);
}

// mappa chiavi array
function fatt_24_array_map_kv($f, array $a) {
	return array_map(function($k) use ($f, $a) {
		return $f($k, $a[$k]);
	}, array_keys($a));
}

// mappa valori array
function fatt_24_array_map_values($f, array $a) {
    return array_values(array_map($f, $a));
}

function fatt_24_array_map_kv_values($f, array $a) {
   return array_values(fatt_24_array_map_kv($f, $a));
}

// converte array in stringa
function fatt_24_array_string($m, $sep = '') {
    return is_array($m) ? implode($sep, $m) : $m;
}
/* funzioni commentate per inutilizzo e futura cancellazione Davide Iandoli 11.02.2020
// restituisco un array di array
function fatt_24_aggregate_array(array $array, $key_extract) {
    $aggregate = array();

    foreach($array as $value) {
        $k = $key_extract($value);
        if (!isset($aggregate[$k]))
            $aggregate[$k] = array();
        $aggregate[$k][] = $value;
    }
    return $aggregate;
}

function fatt_24_aggregate_names_store(array &$aggregate, $index, array $parts) {
    $N = array_shift($parts);
    if (!count($parts))
        $aggregate[$N][] = $index;
    else {
        if (!isset($aggregate[$N]))
            $aggregate[$N] = array();
        fatt_24_aggregate_names_store($aggregate[$N], $index, $parts);
    }
}

function fatt_24_array_column(array $a, $c, $k = null) {
    $ret = array();
    if ($k)
       foreach($a as $r)
         $ret[$r[$k]] = $r[$c];
    else
       foreach($a as $r)
         $ret[] = $r[$c];
    return $ret;
}

function fatt_24_numeric_keys_array(array $array, $start = 0, $stride = 1) {
	$r = array();
	foreach ($array as $v) {
		$r[$start] = $v;
		$start += $stride;
	}
	return $r;
}

function fatt_24_position_in_array($needle, array $haystack) {
    $k = array_search($needle, $haystack);
    return $k === false ? false : array_search($k, array_keys($haystack));
}

function fatt_24_position_in_array_keys($needle, array $haystack) {
    return array_search($needle, array_keys($haystack));
}
*/
// con questi metodi costruisco i tag html
function fatt_24_attr($k, $v) { return array( $k => $v ); }
function fatt_24_align($pos) { return fatt_24_attr('text-align', $pos); }
function fatt_24_klass($klass) { return fatt_24_attr('class', $klass); }
function fatt_24_title($title) { return fatt_24_attr('title', $title); }
function fatt_24_id($id) { return fatt_24_attr('id', $id); }
function fatt_24_style(array $properties) {
    return fatt_24_attr('style', implode(';', fatt_24_array_map_kv(function($k, $v) { return is_int($k) ? $v : "$k:$v"; }, $properties)));
}

function fatt_24_attributes(array $attrs) {
    return fatt_24_array_map_kv(function($k, $v) { return is_int($k) ? $v : "$k=\"$v\""; }, $attrs);
}

/**
 * Costruisce tag html ben formati
 */
function fatt_24_tag($tag, $A1, $A2) {
    if (isset($A2)) { // $A1: attributes
        if (is_array($A1))
            $A1 = fatt_24_array_string(fatt_24_attributes($A1), ' ');
        return sprintf('<%s %s>%s</%s>', $tag, $A1, fatt_24_array_string($A2, "\n"), $tag);
    }
    return sprintf('<%s>%s</%s>', $tag, fatt_24_array_string($A1, "\n"), $tag);
}

function fatt_24_a      ($A1, $A2 = null) { return fatt_24_tag('a', $A1, $A2); }
function fatt_24_b      ($A1, $A2 = null) { return fatt_24_tag('b', $A1, $A2); }
function fatt_24_i      ($A1, $A2 = null) { return fatt_24_tag('i', $A1, $A2); }
function fatt_24_p      ($A1, $A2 = null) { return fatt_24_tag('p', $A1, $A2); }
function fatt_24_th     ($A1, $A2 = null) { return fatt_24_tag('th', $A1, $A2); }
function fatt_24_tr     ($A1, $A2 = null) { return fatt_24_tag('tr', $A1, $A2); }
function fatt_24_td     ($A1, $A2 = null) { return fatt_24_tag('td', $A1, $A2); }
function fatt_24_h1     ($A1, $A2 = null) { return fatt_24_tag('h1', $A1, $A2); }
function fatt_24_h2     ($A1, $A2 = null) { return fatt_24_tag('h2', $A1, $A2); }
function fatt_24_h3     ($A1, $A2 = null) { return fatt_24_tag('h3', $A1, $A2); } // aggiunta funzione tag h3
function fatt_24_h4     ($A1, $A2 = null) { return fatt_24_tag('h4', $A1, $A2); }
function fatt_24_h5     ($A1, $A2 = null) { return fatt_24_tag('h5', $A1, $A2); }
function fatt_24_h6     ($A1, $A2 = null) { return fatt_24_tag('h6', $A1, $A2); }
function fatt_24_ul     ($A1, $A2 = null) { return fatt_24_tag('ul', $A1, $A2); }
function fatt_24_li     ($A1, $A2 = null) { return fatt_24_tag('li', $A1, $A2); }
function fatt_24_div    ($A1, $A2 = null) { return fatt_24_tag('div', $A1, $A2); }
function fatt_24_img    ($A1, $A2 = null) { return fatt_24_tag('img', $A1, $A2); }
function fatt_24_pre    ($A1, $A2 = null) { return fatt_24_tag('pre', $A1, $A2); }
function fatt_24_span   ($A1, $A2 = null) { return fatt_24_tag('span', $A1, $A2); }
function fatt_24_strong ($A1, $A2 = null) { return fatt_24_tag('strong', $A1, $A2); }
function fatt_24_table  ($A1, $A2 = null) { return fatt_24_tag('table', $A1, $A2); }
function fatt_24_thead  ($A1, $A2 = null) { return fatt_24_tag('thead', $A1, $A2); }
function fatt_24_tbody  ($A1, $A2 = null) { return fatt_24_tag('tbody', $A1, $A2); }
function fatt_24_label  ($A1, $A2 = null) { return fatt_24_tag('label', $A1, $A2); }
function fatt_24_select ($A1, $A2 = null) { return fatt_24_tag('select', $A1, $A2); }
function fatt_24_option ($A1, $A2 = null) { return fatt_24_tag('option', $A1, $A2); }
function fatt_24_optgroup($A1, $A2 = null) { return fatt_24_tag('optgroup', $A1, $A2); }

// tag html input
function fatt_24_input  ($A1) {
    if (is_array($A1))
        $A1 = fatt_24_array_string(fatt_24_attributes($A1), ' ');
    return sprintf('<input %s>', $A1);
}
// tag html radio
function fatt_24_radio($group, $value) { 
    return fatt_24_input(array('type'=>"radio", 'name'=>$group, 'value'=>$value),array()); 
}

// creo pulsante con una classe personalizzata (se definita)
function fatt_24_button($action, $caption, $class = null) {
    return fatt_24_tag('button', array('class'=> $class, 'onclick' => $action), $caption);
}    

// crea pulsante utilizzanto il tag input
function fatt_24_btn($action, $caption) {
    return fatt_24_div(array(
        fatt_24_input(array('id' => $action.'_btn') + array('type' => 'button', 'value' => $caption, 'class' => 'button')),
    ));
}
// associa un comando 
function fatt_24_cmd($cmd, $label, $desc = null) {
    return array( 'id' => $cmd, 'type' => 'content', 'content' => fatt_24_btn($cmd, $label), 'desc' => $desc );
}
/* funzioni commentate per inutilizzo e futura cancellazione Davide Iandoli 11.02.2020
function fatt_24_label_slug($Label) {
    return strtolower(str_replace(' ', '-', $Label));
}

// tabella con intestazione
function fatt_24_table_with_header(array $t_array, $ID) {
    $html[] = fatt_24_tr(array_map(function($k) { return fatt_24_th($k); }, array_keys((array)$t_array[0])));
    foreach ($t_array as $r)
        $html[] = array_map(function($e) { return fatt_24_td($e); }, $r);
    return fatt_24_table(id($ID), $html);
}

function fatt_24_css($k, $css) {
    if (substr($css, 0, 4) == 'http')
        wp_enqueue_style($k, $css.'.css');
    else
        wp_enqueue_style($k, fatt_24_url($css.'.css'));
}

function fatt_24_script($k, $script, $dep) {
    $req = $dep ? $dep : array();
    wp_enqueue_script($k, fatt_24_url($script), $req);
}

function fatt_24_js_($k, $js, $dep) {
    fatt_24_script($k, $js.'.js', $dep);
}
*/
// restituisce l'indirizzo url di un file
function fatt_24_url($resource) {
    $url = plugins_url($resource, __FILE__);
    return $url;
}

// carica immagini png
function fatt_24_png($path) {
   return fatt_24_url($path.'.png');
}
/* funzioni commentate per inutilizzo e futura cancellazione
function fatt_24_bulk_operation_optimize($on_off) {
	wp_defer_term_counting($on_off);
	wp_defer_comment_counting($on_off);
	global $wpdb;

    if ($on_off) {
        $wpdb->query('set autocommit=0;');
    } else {
        $wpdb->query('commit;');
		$wpdb->query('set autocommit=1;');
	}
}

function fatt_24_inc_key(array &$counters, $key, $val = 1) {
    if (isset($counters[$key]))
        $counters[$key] += $val;
    else
        $counters[$key] = $val;
}

function fatt_24_array_2_table(array $a) {
    return fatt_24_table(fatt_24_array_map_kv(function($k,$v) { return fatt_24_tr(array(fatt_24_td($k),fatt_24_td($v))); }, $a));
}

function fatt_24_scan_1($val, $spec) {
    if ($val = sscanf($val, $spec))
        return $val[0];
}

function fatt_24_assign_if_not($type, $if_not, $not_overwrite) {
    return array_search($type, $not_overwrite) === false ? $if_not : $type;
}

function fatt_24_rgb($color, $inner) { 
    return fatt_24_div(fatt_24_style(array('color'=>$color)), $inner); 
}

function fatt_24_ok() { 
    return fatt_24_rgb('green', 'ok'); 
}

function fatt_24_ko() { 
    return fatt_24_rgb('red', 'ko'); 
}

function fatt_24_flag($ok) { 
    return fatt_24_b($ok ? fatt_24_ok() : fatt_24_ko()); 
}

function fatt_24_ver($min) { 
    return fatt_24_flag(phpversion() >= $min); 
}

function fatt_24_ext($ext) { 
    return fatt_24_flag(extension_loaded($ext)); 
}

function fatt_24_has_gd()  { 
    return fatt_24_flag(fatt_24_ext('GD') || is_array(gd_info())); 
}

function fatt_24_capture_html($function, $args = null) {
	ob_start();
	call_user_func($function, $args);
   	$html = ob_get_contents();
    ob_end_clean();
	return $html;
}*/
