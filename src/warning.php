<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: Tab "Attenzione" della schermata di impostazioni
 * 
 */

namespace fattura24;

if (!defined('ABSPATH'))
    exit;

/*if (is_admin())
{
    //@session_start();
}*/

// visualizza la tab "Attenzione"
function show_warning() {
    fatt_24_get_link_and_logo(__('', 'fatt-24-warning'));
    ?>

    <div class='wrap'>
    
       <nav class="nav-tab-wrapper woo-nav-tab-wrapper">
            <a href="?page=fatt-24-settings"  class="nav-tab "><?php _e('General Settings', 'fattura24')?></a>	
            <a href="?page=fatt-24-tax" class="nav-tab"><?php _e('Tax Configuration', 'fattura24')?></a>
			<a href="?page=fatt-24-app" class="nav-tab"><?php _e('Mobile App', 'fattura24')?></a>
            <a href="?page=fatt-24-warning" class="nav-tab nav-tab-active"><?php echo fatt_24_notice(); _e(' Warning', 'fattura24')?></a>
       </nav> 
       
       <div style = "text-align:center;">
            <h1>Novità fiscale importantissima</h1>
            <h2>Circolare Agenzia Entrate n. 14/E del 17.06.2019</h2>
       </div>
       
       <p style="font-size: 120%; text-align:justify;">L'Agenzia delle Entrate ha emesso, in data 17 giugno 2019, la Circolare n. 14/E denominata 
       <strong><em>"Chiarimenti in tema di documentazione di operazioni rilevanti ai fini IVA, alla luce dei recenti interventi normativi in tema di fatturazione elettronica".</em></strong>
       Questo nuovo intervento cerca di dare finalmente un’interpretazione unitaria delle più recenti innovazioni legislative in tema di IVA e di fatturazione elettronica e attesta in sostanza l’avvio del nuovo sistema di fatturazione e di datazione ed emissione fatture, con sicuro effetto <strong>dal 1 luglio 2019</strong>, e che sarà la regola per il futuro. <br /><br />
       Queste nuove norme prevedono quanto segue. <br /><br />
 
       Il comma 2, lett. <strong>g-bis</strong> vuole ora che nella fattura elettronica si deve indicare, <u>OLTRE</u> alle altre tradizionali e consuete indicazioni (le prime due sono la <em>‘data di emissione’</em> e il <em>‘numero’</em> della fattura richieste delle lettere <em>a</em> e <em>b</em>):
       <div style = "font-size: 120%; margin-left:100px;">
            <ul style = "list-style-type:disc;">
                <li>la "<strong><em>data</strong> in cui è effettuata la <em>cessione di beni</em> o la <em>prestazione di servizi</em> ovvero la data in cui è corrisposto in tutto o in parte il corrispettivo, <em>sempreché</em> tale data sia diversa dalla data di emissione della fattura";</em></li>
            </ul>
       </div>

       <p style="font-size: 120%; text-align:justify;">Il nuovo comma 4, primo periodo, invece prevede che la fattura può essere emessa:</p>

       <div style = "font-size: 120%; margin-left:100px;">
            <ul style = "list-style-type:disc;">
                <li>"<strong><em>entro dieci giorni</strong> dall'effettuazione dell'operazione determinata ai sensi dell'art.6".</em></li>
            </ul>
       </div>
       
       <p style="font-size: 120%; text-align:justify;">
       L’Agenzia, allora, preso atto di queste nuove disposizioni e cambiamenti, ha affermato che, poiché <em>“per una fattura elettronica veicolata attraverso lo Sdi quest’ultimo ne attesta inequivocabilmente ... la data ... di avvenuta trasmissione”</em>, allora <em>“è possibile assumere che la data riportata nel campo <strong>‘Data’</strong> della sezione ‘Dati Generali’ del file della fattura elettronica sia <strong><u>sempre e comunque</u> la data di <u>effettuazione dell’operazione</u>”.</strong></em>
       </p>
       <p style="font-size: 120%; text-align:justify;">
       Prosegue quindi l’Agenzia rilevando che <em>“anche se l’operatore decidesse di ‘emettere’ la fattura elettronica via Sdi <strong>non</strong> entro le 24 ore dal giorno dell’Operazione, <strong>bensì</strong> in uno dei successivi 10 giorni previsti dal novellato articolo 21 ..., <strong>la data del documento dovrà sempre essere valorizzata con la data dell’<u>operazione</u></strong> e i 10 giorni citati potranno essere sfruttati per la trasmissione del file della fattura elettronica al Sistema di Interscambio”.</em>
Per quanto concerne la <strong>fattura differita</strong>, si anticipa fin da qui che, secondo l’Agenzia, questo tipo di documento si crea <em>“valorizzando <strong>la data</strong> della fattura (campo ‘Data’ della sezione ‘Dati Generali’ del file) <strong>con la data dell’ultima operazione</strong>“</em> caricata in fattura (ad esempio con la data dell’ultimo dei DDT indicati nella fattura).<br /><br />
Nota di F24: Tenuto conto della novità e complessità dei temi introdotti dalla nuova Circolare, vi invitiamo comunque a confrontarvi con il commercialista per verificare ed organizzare correttamente le vostre modalità e tempistiche di fatturazione.
</p>
<?php  }